angular.module('keitoApp').factory('createTeamService', [ '$http','$q','utilityService','$rootScope',
  function($http,$q,utilityService,$rootScope) {
    //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";


    var createteam_url = "http://test-apis.keito.in/team";
    var getmyteam_url = "http://test-apis.keito.in/team";
    var getmyteamByID_url = "http://test-apis.keito.in/team/";
    var addMemberToTeam_url = "http://test-apis.keito.in/team/";
    var removeMemberToTeam_url = "http://test-apis.keito.in/team/:id/members";
    var acceptRejectInvitation_url = "http://test-apis.keito.in/team/";
    var getContacts_url = "http://test-apis.keito.in/contacts";
    var updateUserProfileToTeam_url = "http://test-apis.keito.in/team/";
    return {   
          getmyteam: function (token){
                var deferred = $q.defer();
                 $http({
                            method: "GET",
                            headers: {'Content-Type': 'application/json;','AUTH-TOKEN':token},
                            url:getmyteam_url
                        })
                   .success(function(data) { 
                      deferred.resolve(data);

                   }).error(function(msg, code) {
                      deferred.reject(msg);
                      $rootScope.progressBarShow = false;
                      utilityService.serviceError("Service Error, Please Contact Administrator.");
                   });
                return deferred.promise;

          },
          getContacts: function (token){
              var deferred = $q.defer();
                 $http({
                            method: "GET",
                            headers: {'Content-Type': 'application/json;','AUTH-TOKEN':token},
                            url:getContacts_url
                        })
                   .success(function(data) { 
                      deferred.resolve(data);

                   }).error(function(msg, code) {
                      deferred.reject(msg);
                      $rootScope.progressBarShow = false;
                      utilityService.serviceError("Service Error, Please Contact Administrator.");
                   });
                return deferred.promise;
          },
          createteam: function(token,data) {
              var deferred = $q.defer();
                 $http({
                            method: "POST",
                            headers: {'Content-Type': 'application/json;','AUTH-TOKEN':token},
                            url:createteam_url,
                            data: data
                        })
                   .success(function(data) { 
                      deferred.resolve(data);

                   }).error(function(msg, code) {
                      deferred.reject(msg);
                      $rootScope.progressBarShow = false;
                      utilityService.serviceError("Service Error, Please Contact Administrator.");
                   });
                return deferred.promise;
          },
          getTeamById: function(token,id) {
              var deferred = $q.defer();
                 $http({
                            method: "GET",
                            headers: {'Content-Type': 'application/json;','AUTH-TOKEN':token},
                            url:getmyteamByID_url+""+id
                        })
                   .success(function(data) { 
                      deferred.resolve(data);

                   }).error(function(msg, code) {
                      deferred.reject(msg);
                      $rootScope.progressBarShow = false;
                      utilityService.serviceError("Service Error, Please Contact Administrator.");
                   });
                return deferred.promise;
          },
          updateUserProfileToTeam : function(token,id,data) {
                 var deferred = $q.defer();
                 $http({
                            method: "PUT",
                            headers: {'Content-Type': 'application/json;','AUTH-TOKEN':token},
                            url:updateUserProfileToTeam_url+""+id+""+"/profile",
                            data: data
                        })
                   .success(function(data) { 
                      deferred.resolve(data);

                   }).error(function(msg, code) {
                      deferred.reject(msg);
                      $rootScope.progressBarShow = false;
                      utilityService.serviceError("Service Error, Please Contact Administrator.");
                   });
                return deferred.promise;
          },
          addTeamMembers: function(token,id,data) {
                 var deferred = $q.defer();
                 $http({
                            method: "PUT",
                            headers: {'Content-Type': 'application/json;','AUTH-TOKEN':token},
                            url:addMemberToTeam_url+""+id+""+"/members",
                            data: data
                        })
                   .success(function(data) { 
                      deferred.resolve(data);

                   }).error(function(msg, code) {
                      deferred.reject(msg);
                      $rootScope.progressBarShow = false;
                      utilityService.serviceError("Service Error, Please Contact Administrator.");
                   });
                return deferred.promise;
          },
          acceptRejectInvitation: function(token,id,action) {
              var deferred = $q.defer();
                 $http({
                            method: "PUT",
                            headers: {'Content-Type': 'application/json;','AUTH-TOKEN':token},
                            url:addMemberToTeam_url+""+id+""+"/invitation/"+action
                        })
                   .success(function(data) { 
                      deferred.resolve(data);

                   }).error(function(msg, code) {
                      deferred.reject(msg);
                      $rootScope.progressBarShow = false;
                      utilityService.serviceError("Service Error, Please Contact Administrator.");
                   });
                return deferred.promise;
          }
    }

}]);

/*
$http({
  method: 'GET',
  url: '/someUrl'
}).then(function successCallback(response) {
    // this callback will be called asynchronously
    // when the response is available
  }, function errorCallback(response) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });

*/