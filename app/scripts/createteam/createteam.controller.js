angular.module('keitoApp').controller('createTeamCtrl', function ($scope,createTeamService,$rootScope,$http,$state,$uibModal,$log,utilityService) {
   
   $scope.teams = undefined;
   
   $rootScope.progressBarText1 = "Loading Teams and Projects";
   $rootScope.progressBarText2 = "Create new teams";
   $rootScope.progressBarText3 = "Add members to teams";
   $rootScope.progressBarShow = true;

   $scope.members = [];
   $scope.membersEmailId = [];
   $scope.membersMobile = [];

   $scope.loadContacts = function() {
     $scope.contactCheckboxValues= [];
     $scope.selectedContactId = [];
     $scope.selectedContactObjects = [];
   
   $rootScope.progressBarText1 = "Loading Teams";
     $rootScope.progressBarText2 = "Fetching Contacts";
     $rootScope.progressBarText3 = "Loading Teams";
     $rootScope.progressBarShow = true;
     createTeamService.getContacts($rootScope.AUTHTOKEN).then(function(response) {
               $rootScope.progressBarShow = false;
               $scope.showContacts=true;
               $scope.contacts =response;    

               console.log($scope.contacts);
            })
   }


   $scope.sendContacts = function() {
    $scope.showContacts=false;
    for(i=0;i<$scope.selectedContactObjects.length;i++) {
      if($scope.members.indexOf($scope.selectedContactObjects[i].mobileNumber) == -1) {
        $scope.members.push($scope.selectedContactObjects[i].mobileNumber);
        $scope.membersMobile.push($scope.selectedContactObjects[i].mobileNumber);
      }
    }
   }

   $scope.hideContactsDiv = function() {
    $scope.showContacts=false;
    $scope.selectedContactObjects = [];
    $scope.selectedContactId = [];
   }

   $scope.contactCheckboxValues= [];

   $scope.selectedContactId = [];
   $scope.selectedContactObjects = [];
   
   $scope.pushContact = function(contact) {
      if($scope.selectedContactId.indexOf(contact.id) == -1) {
        $scope.selectedContactId.push(contact.id);
        $scope.selectedContactObjects.push(contact);
        $scope.contactCheckboxValues[contact.id] = true;
      }
      else {
         $scope.selectedContactId.splice($scope.selectedContactId.indexOf(contact.id),1);
        $scope.selectedContactObjects.splice($scope.selectedContactId.indexOf(contact.id),1);
        $scope.contactCheckboxValues[contact.id] = false;
      }
   }
    
    createTeamService.getmyteam($rootScope.AUTHTOKEN).then(function(response) {
         $rootScope.progressBarShow = false;
         $scope.teams =response;
         console.log(response);      
      })

    $scope.showcreateTeam =  false;

    $scope.createTeam = function() {
    	$scope.showcreateTeam = !$scope.showcreateTeam;
      $scope.loadContacts();
    }

    $scope.invitationAction = function(action,id) {
      

      if(action == "reject") {
        var modalInstance = utilityService.showConfirm("Are you sure to reject this team invitation?");

        modalInstance.result.then(function (selectedItem) {
              console.log(selectedItem);
              $log.info('Modal dismissed at: ' + new Date());

            }, function (selectedItem) {
              if(selectedItem == true) {
                $rootScope.progressBarText1 = "Loading Teams and Projects";
                $rootScope.progressBarText2 = "Create new teams";
                $rootScope.progressBarText3 = "Add members to teams";
                $rootScope.progressBarShow = true;
                createTeamService.acceptRejectInvitation($rootScope.AUTHTOKEN,id,action).then(function(response) {
                   //$rootScope.progressBarShow = false;
                    createTeamService.getmyteam($rootScope.AUTHTOKEN).then(function(response) {
                       $rootScope.progressBarShow = false;
                       $scope.teams =response;    
                    })
                })
              }
            });
      }
      else {
        $rootScope.progressBarText1 = "Loading Teams and Projects";
        $rootScope.progressBarText2 = "Create new teams";
        $rootScope.progressBarText3 = "Add members to teams";
        $rootScope.progressBarShow = true;
        createTeamService.acceptRejectInvitation($rootScope.AUTHTOKEN,id,action).then(function(response) {
           //$rootScope.progressBarShow = false;
            createTeamService.getmyteam($rootScope.AUTHTOKEN).then(function(response) {
               $rootScope.progressBarShow = false;
               $scope.teams =response;    
            })
        })
      }

      //console.log(action);
      
    }

    $scope.addTeamMember = function(id) {
      $scope.showcreateTeam = !$scope.showcreateTeam;
      $scope.editTeam = true;

      $rootScope.progressBarText1 = "Loading Teams Details";
      $rootScope.progressBarText2 = "Adding members to team";
      $rootScope.progressBarText3 = "Removing member at one shot";
      $rootScope.progressBarShow = true;
      createTeamService.getTeamById($rootScope.AUTHTOKEN,id).then(function(response) {
         $rootScope.progressBarShow = false;
          var teamDetails =response;
          $scope.teamname = teamDetails.name;
          $scope.description = teamDetails.description;
          $scope.editTeamId= teamDetails.id;
          $scope.createdBy = teamDetails.createdByName;
          $scope.teamMembers = teamDetails.members;
      })
    }

    

    $scope.cancelAddingTeam = function() {
      $scope.editTeam = false;
    	$scope.showcreateTeam =  !$scope.showcreateTeam;
      $scope.showContacts = false;
      $scope.members = [];
      $scope.membersEmailId = [];
      $scope.membersMobile = [];
      $scope.description = undefined;
      $scope.teamname = undefined;
    }

    $scope.addTeam = function() {
    	
    	if($scope.teamname == undefined || $scope.teamname == null) {
    		utilityService.serviceError("Please Enter Team Name");
    		return;
    	}
    	if($scope.description == undefined || $scope.description == null) {
    		utilityService.serviceError("Please Enter Description");
    		return;
    	}

      console.log($scope.membersMobile);
      console.log($scope.membersEmailId);

      var obj = {
        "name" : $scope.teamname,
        "description" : $scope.description,
      	"memberMobileNumbers" : $scope.membersMobile,
        "memberEmails" : $scope.membersEmailId,
      }

      $rootScope.progressBarText1 = "Creating new team";
      $rootScope.progressBarText2 = "Sending invites";
      $rootScope.progressBarText3 = "Waiting for joining";
      $rootScope.progressBarShow = true;
      createTeamService.createteam($rootScope.AUTHTOKEN,obj).then(function(response) {
         $rootScope.progressBarShow = false;
         console.log(response);
         $scope.members = [];
         $scope.membersEmailId = [];
         $scope.membersMobile = [];
         $scope.description = undefined;
         $scope.teamname = undefined;
         createTeamService.getmyteam($rootScope.AUTHTOKEN).then(function(response) {
         	$scope.teams =response;
         	$scope.showcreateTeam =  !$scope.showcreateTeam;
          $rootScope.teamsForProfile =response;   
         
      })

      })

    }

    $scope.editTeamMembers = function() {
      
      console.log($scope.membersMobile);
      console.log($scope.membersEmailId);
      var obj = {
        "memberMobileNumbers" : $scope.membersMobile,
        "memberEmails" : $scope.membersEmailId
      }

      $rootScope.progressBarShow = true;
      createTeamService.addTeamMembers($rootScope.AUTHTOKEN,$scope.editTeamId,obj).then(function(response) {
         $rootScope.progressBarShow = false;
         console.log(response);
          $scope.members = [];
         $scope.membersEmailId = [];
         $scope.membersMobile = [];
         createTeamService.getmyteam($rootScope.AUTHTOKEN).then(function(response) {
          $scope.editTeam = false;
          $scope.teams =response;
          $scope.showcreateTeam =  !$scope.showcreateTeam;
         
      })

      })

    }

    $scope.removeItem = function(x, y) {
    	$scope.members.splice(x,1);
      if($scope.membersMobile.indexOf(y) != -1){
        $scope.membersMobile.splice($scope.membersMobile.indexOf(y),1);
      
      }
      if($scope.membersEmailId.indexOf(y) != -1){
        $scope.membersEmailId.splice($scope.membersEmailId.indexOf(y),1);
      }
    }

    $scope.inviteUser = function () {
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'views/modals/inviteUser.html',
      controller: 'inviteUserCtrl'
    });

    modalInstance.result.then(function (selectedItem) {
    	
    	if ($scope.members.indexOf(selectedItem.item) > -1) {
		    utilityService.serviceError("Member already exists");
		  } 
		else {
			$scope.members.push(selectedItem.item);
      if(selectedItem.type == "email") {
        $scope.membersEmailId.push(selectedItem.item);
      }
      else {
        $scope.membersMobile.push(selectedItem.item);
      }
 
		}

    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  $scope.showDescription = function(msg) {
    var modalInstance = $uibModal.open({
            templateUrl: 'views/modals/description.html',
            controller: 'descriptionCtrl',
            resolve: {
              msg: function () {
                return msg;
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
                  
            $log.info('Modal dismissed at: ' + new Date());

          }, function () {
            $rootScope.progressBarShow = false;
            $log.info('Modal dismissed at: ' + new Date());
          });
  }


  var windowWith =$(window).outerWidth();
	  var navbarHeight = $('.navbar').outerHeight();
	  var topHeadHeight = $scope.topHeadHeight;
	  var chaticon = $('.my-type-chat-ctrl').outerHeight();
	  var WindowHeight = $(window).outerHeight();
	
	 $('.contact-list').css({"height":WindowHeight - navbarHeight - 200});


});


angular.module('keitoApp').controller('inviteUserCtrl', function ($uibModalInstance,$scope,utilityService) {
  var mobileRegx= /^([7-9][0-9]*)$/
  var emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
   
  $scope.ok = function () {
  	if ($scope.mobile == undefined || $scope.mobile == "") {
      utilityService.serviceError("Enter Valid mobile No or Email Id");
    }
    else {
      if(!emailRegx.test($scope.mobile)) {
        if(!mobileRegx.test($scope.mobile) || $scope.mobile.length != 10){
          utilityService.serviceError("Enter Valid mobile No or Email Id");
        }
        else {

          var obj = {
              "item" : $scope.mobile,
              "type" : "mobile"
          }
           $uibModalInstance.close(obj);
        }
        
      }
      else {
        var obj = {
              "item" : $scope.mobile,
              "type" : "email"
          }
        $uibModalInstance.close(obj);
      }

    }	
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };


});
