angular.module('keitoApp').controller('productcontactsCtrl', function ($scope,$rootScope,createTeamService,utilityService,$state,Users) {
  
  $scope.utilityService = utilityService;
  $rootScope.progressBarText1 = "Loading Teams Details";
  $rootScope.progressBarText2 = "Loading Contacts";
  $rootScope.progressBarText3 = "Loading Members";
  $rootScope.progressBarShow = true;

  $scope.Users = Users;


    createTeamService.getTeamById($rootScope.AUTHTOKEN,$rootScope.currentTeam).then(function(response) {
         $rootScope.progressBarShow = false;
          $scope.teamContacts =response.members;
      })
     $scope.getStatusClass =  function(status) {
          switch(status) {
            case "Available" : return "statusAvailable";
            case "Busy" : return "statusBusy";
            case "Away" : return "statusAway";
            case "Do no disturb" : return "statusDoNotDisturb";
            case "Not At Work" : return "statusNotAtWork"
          }
         }
    $scope.openChat =function(user) {

      if(!user.acceptedInvite) {
        utilityService.serviceError("This User has not accepted Team Invite Yet");
        return;
      }
    	$rootScope.fromContacts = user;

      
    	$state.go('profile.productchats');
    	$scope.producttab('Chats');
    }

    

});