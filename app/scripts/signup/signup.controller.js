angular.module('keitoApp').controller('signUpCtrl', function ($scope,signUpService,$rootScope,$http,$state,utilityService) {
    
    document.getElementById('progressBarShowLoading').style.display = "none";
    document.getElementById('afterLoading').style.display = "";
    
    $rootScope.progressBarShow = false;
    $rootScope.progressBarShowLoading = false;
    $rootScope.mbody = "sbody";
    $rootScope.footerclr = "";
    $rootScope.footerhide = true;

    var emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    var mobileRegx= /^([7-9][0-9]*)$/
    var nameRegex = /^([a-zA-Z ]*)$/
    
    $scope.dateOptions = {
      minDate : new Date('01/01/1992'),
      maxDate : new Date()
    }
       
    $scope.open2 = function() {
      $scope.popup2.opened = true;
      console.log($scope.minDate)
    };

    $scope.popup2 = {
      opened: false
    };

    $scope.signUp = function () {        

        if(!nameRegex.test($scope.name)) {
          utilityService.serviceError("Please Enter Valid Name.");
          return;
        }

        if($scope.gender == null || $scope.gender == undefined) {
          utilityService.serviceError("Please select Gender");
          return;
        }

        if($scope.dob == null || $scope.dob == undefined) {
          utilityService.serviceError("Please enter Date Of Birth");
          return;
        }

        if($scope.country == null || $scope.country == undefined) {
          utilityService.serviceError("Please select country");
          return;
        }

        if($scope.mobile.toString().length !=10 || !mobileRegx.test($scope.mobile)) {
            utilityService.serviceError("Please enter Valid Mobile No.");
            return;
        }

        if(!emailRegx.test($scope.email)) {
            utilityService.serviceError("Please Enter Valid Email Address");
            return;
          }

        var obj = {
        "name" : $scope.name, 
        "mobileNumber" : $scope.mobile.toString(),
        "gender" : $scope.gender,
        "dateOfBirth" : $scope.dob.toISOString().slice(0,10),
        "email" : $scope.email
      }
      console.log(obj);
      $rootScope.progressBarShow = true;

      signUpService.signUp(obj).then(function(response){
        $rootScope.progressBarShow = false;
        

        utilityService.serviceError("Successfully Joined. Please enter otp received to login");

        var obj = {
        "mobileNumber" : $scope.mobile
        }

        $scope.country = undefined;
        $scope.email = undefined;
        $scope.mobile = undefined;
        $scope.dob = undefined;
        $scope.gender = undefined;

        $rootScope.progressBarText1 = "Loading";
        $rootScope.progressBarText2 = "Handling Data";
        $rootScope.progressBarText3 = "Sending OTP";
        $rootScope.progressBarShow = true;
        signInService.sendOtp(obj).then(function(response) {
          $rootScope.progressBarShow = false;
          $scope.otp = response.otp;
          $state.go('loginOtp', { "otp" : $scope.otp, "mobile": obj.mobileNumber});
          
        })

      })

    }



});
