angular.module('keitoApp').factory('signUpService', [ '$http','$q','utilityService',
  function($http,$q,utilityService) {
    //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";


    var post_url = "http://test-apis.keito.in/user";
     return {   
      signUp: function (postDetail){
                var deferred = $q.defer();
                 $http({
                            method: "POST",
                            headers: {'Content-Type': 'application/json;'},
                            url:post_url,
                            data: postDetail
                        })
                   .success(function(data) { 
                      deferred.resolve(data);

                   }).error(function(msg, code) {
                      deferred.reject(msg);
                      utilityService.serviceError(msg.errors[0]);
                   });
                 return deferred.promise;


              /*return $http({
                  method: "POST",
                  headers: {'Content-Type': 'application/json;'},
                  url:post_url,
                  data: postDetail
              }).then(successCallback,errorCallback);*/
            }
    }

}]);

/*
$http({
  method: 'GET',
  url: '/someUrl'
}).then(function successCallback(response) {
    // this callback will be called asynchronously
    // when the response is available
  }, function errorCallback(response) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });

*/