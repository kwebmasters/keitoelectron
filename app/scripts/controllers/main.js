'use strict';

/**
 * @ngdoc function
 * @name keitoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the keitoApp
 */


 angular.module('keitoApp').directive('focusOn', function() {
   return function(scope, elem, attr) {
      scope.$on('focusOn', function(e, name) {
        if(name === attr.focusOn) {
          elem[0].focus();
        }
      });
   };
});

angular.module('keitoApp').factory('focus', function ($rootScope, $timeout) {
  return function(name) {
    $timeout(function (){
      $rootScope.$broadcast('focusOn', name);
    });
  }
});


angular.module('keitoApp')
  .controller('MainCtrl', function ($rootScope,$firebaseObject,$location,$scope,$log,$uibModal,$timeout,focus,utilityService) {

    $(".navbar").addClass("addHeaderDeactiveClass");

    $rootScope.footerclr = "footerClr";


    $rootScope.GettingStartedemailId = "Enter your email...";
    
    
    $rootScope.GettingStartedBtnTxt = "Request Early Access";

    $rootScope.outOfGettingStartedFocus = function() {
      if($rootScope.GettingStartedemailId == "" || $rootScope.GettingStartedemailId == undefined || $rootScope.GettingStartedemailId == null) {
        $rootScope.GettingStartedemailId = "Enter your email...";
      }
    }

    $rootScope.clearGSEmail = function() {
      $rootScope.GettingStartedemailId = "";
    }

  $rootScope.GettingStartedemailIdFocus = function() {
    $rootScope.GettingStartedemailId = "";
    focus('focusMe'); 
  }

  var sendData = function(type, URL, formData, callBack){
          var xhr = new XMLHttpRequest();
          xhr.open(type, URL, true);
          xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=ISO-8859-1')
          xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
              var response = xhr.responseText;
              callBack(response);
            }
          };
          if(type == "GET"){
            xhr.send();
          }
          if(type == "POST"){
            xhr.send(formData);
          }
        };


  $rootScope.GettingStartedClicked = function(mailId) {
    var email = mailId;
    var emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    

    if($rootScope.GettingStartedBtnTxt == "Request Early Access") {
      if(!emailRegx.test(email)) {
            utilityService.serviceError("Please Enter Valid Email Address");
            return;
          }

        $rootScope.myValue = "move";
        $rootScope.GettingStartedBtnTxt = "";
        $timeout(function() {
          $rootScope.GettingStartedBtnTxt = "Thank You"
        }, 2000); 

        var url = "./lib/send-mail.php";

          var from = email + "<" + email + ">";
          var to = "TO NAME <amal@pencilanderaser.in>";
          var sub = "PnE subject";
          var msg = 'Contact Us';
          var formdata = "from="+from+"&to="+to+"&sub="+sub+"&msg="+msg+"";

          sendData("POST", url, formdata, function(r){
            console.log(r);
          });

    }
    
  }

  $rootScope.subscribe = function () {
      //$rootScope.prevMbody = $rootScope.mbody
      
      
      var modalInstance = $uibModal.open({
        //animation: $scope.animationsEnabled,
        templateUrl: 'views/modals/subscribe.html',
        controller: 'ModalSubscribeCtrl'
      });
      $rootScope.blurClass = "blurmbody";
      modalInstance.result.then(function (selectedItem) {
        $log.info('Modal dismissed at: ' + new Date());
        $rootScope.blurClass = "";
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
        $rootScope.blurClass = "";
      });
  };
    





    $rootScope.progressBarShow = false;
    $rootScope.mbody = "mbody";
    //$rootScope.footerclr = "";
    $rootScope.footerhide = false;

    $rootScope.progressBarShowLoading = false;

    document.getElementById('afterLoading').style.display = "";
    document.getElementById('progressBarShowLoading').style.display = "none";

    $rootScope.progressBarShow = false;
    $rootScope.progressBarShowLoading = false;

    $('.carousel').carousel({
      interval: 6000,
      pause: "false"
    });

    $scope.changeCarousel = function(index) {
      $('.carousel').carousel(index);
    }

    $rootScope.serviceError = function(msg, code) {
      alert(msg, code);
    }

  });

angular.module('keitoApp')
  .controller('footerCtrl', function ($rootScope,$firebaseObject) {
    $rootScope.footerhide = false;
    document.getElementById('afterLoading').style.display = "";
    document.getElementById('progressBarShowLoading').style.display = "none";

    $rootScope.progressBarShow = false;
    $rootScope.progressBarShowLoading = false;
    
});


angular.module('keitoApp').controller('ModalSubscribeCtrl', function ($uibModalInstance,$scope,$timeout,utilityService) {
  //$scope.items = items;
  

  $rootScope.GettingStartedemailId = "Enter your email...";
    
    
    $rootScope.GettingStartedBtnTxt = "Request Early Access";

  $rootScope.GettingStartedemailIdFocus = function() {
    $rootScope.GettingStartedemailId = "";
    focus('focusMe'); 
  }

  $rootScope.outOfGettingStartedFocus = function() {
      if($rootScope.GettingStartedemailId == "" || $rootScope.GettingStartedemailId == undefined || $rootScope.GettingStartedemailId == null) {
        $rootScope.GettingStartedemailId = "Enter your email...";
      }
    }

    $rootScope.clearGSEmail = function() {
      $rootScope.GettingStartedemailId = "";
    }

  var sendData = function(type, URL, formData, callBack){
          var xhr = new XMLHttpRequest();
          xhr.open(type, URL, true);
          xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=ISO-8859-1')
          xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
              var response = xhr.responseText;
              callBack(response);
            }
          };
          if(type == "GET"){
            xhr.send();
          }
          if(type == "POST"){
            xhr.send(formData);
          }
        };


  $rootScope.GettingStartedClicked = function(email) {
    var emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    

    if($rootScope.GettingStartedBtnTxt == "Request Early Access") {
      if(!emailRegx.test(email)) {
            utilityService.serviceError("Please Enter Valid Email Address");
            return;
          }

        $rootScope.myValue = "move";
        $rootScope.GettingStartedBtnTxt = "";
        $timeout(function() {
          $rootScope.GettingStartedBtnTxt = "Thank You"
        }, 2000);

        var url = "./lib/send-mail.php";

          var from = email + "<" + email + ">";
          var to = "TO NAME <amal@pencilanderaser.in>";
          var sub = "PnE subject";
          var msg = 'Contact Us';
          var formdata = "from="+from+"&to="+to+"&sub="+sub+"&msg="+msg+"";

          sendData("POST", url, formdata, function(r){
            console.log(r);
          }); 
    }
    
  }


  $scope.ok = function () {
    $uibModalInstance.close("selectedUser");
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});


angular.module('keitoApp').controller('ModalChatsCtrl', function ($uibModalInstance,$scope,items) {
  $scope.items = items;
  $scope.ok = function () {
    $uibModalInstance.close($scope.selectedUser);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});


angular.module('keitoApp').controller('ModalDeleteCtrl', function ($uibModalInstance,$scope,items,utilityService) {
  $scope.items = items;
  $scope.ok = function () {
    $uibModalInstance.close('ok');
    //$uibModalInstance.close($scope.selectedUser);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});

angular.module('keitoApp')
  .controller('productCtrl', function ($rootScope,$scope,$uibModal,$log,focus,$timeout) {
    //$scope.trick = "dz";
    //$scope.trick = "";
    $(".navbar").removeClass("addHeaderActiveClassWhite");
    $(".navbar").removeClass("addHeaderActiveClass");

    $(".navbar").addClass("addHeaderDeactiveClass");

    $(function() {
      $(window).on("scroll", function() {
          if($(window).scrollTop() > 50) {
            $(".navbar").removeClass("addHeaderActiveClassWhite");
            $(".navbar").removeClass("addHeaderDeactiveClass");
              $(".navbar").addClass("addHeaderActiveClass");
          } else {
              //remove the background property so it comes transparent again (defined in your css)
             $(".navbar").addClass("addHeaderDeactiveClass");
             $(".navbar").removeClass("addHeaderActiveClass");
          }
      });
  });

    $rootScope.subscribe = function () {
      //$rootScope.prevMbody = $rootScope.mbody
      

      var modalInstance = $uibModal.open({
        //animation: $scope.animationsEnabled,
        templateUrl: 'views/modals/subscribe.html',
        controller: 'ModalSubscribeCtrl'
      });
      $rootScope.blurClass = "blurmbody";
      modalInstance.result.then(function (selectedItem) {
        $log.info('Modal dismissed at: ' + new Date());
        $rootScope.blurClass = "";
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
        $rootScope.blurClass = "";
      });
  };

  $scope.currentOfferingChange = function(value) {
    $scope.currentOffering = value;
  }

  $scope.currentOfferingChangeMouseEnter = function(value) {
    $scope.currentOfferingmouseEnter = value;
  }


    $rootScope.mbody = "pbody";
    $rootScope.footerclr = "";
    $rootScope.footerhide = false;
    $scope.prev;

      $scope.activeDiv = 'cactive';

    document.getElementById('afterLoading').style.display = "";
    document.getElementById('progressBarShowLoading').style.display = "none";

    $rootScope.progressBarShow = false;
    $rootScope.progressBarShowLoading = false;
    
	$scope.displayImage = function(val)  {
      $scope.activeDiv = val;
    }

  $rootScope.GettingStartedemailId = "Enter your email...";
    
    
    $rootScope.GettingStartedBtnTxt = "Request Early Access";

  $rootScope.GettingStartedemailIdFocus = function() {
    $rootScope.GettingStartedemailId = "";
    focus('focusMe'); 
  }

  $rootScope.outOfGettingStartedFocus = function() {
      if($rootScope.GettingStartedemailId == "" || $rootScope.GettingStartedemailId == undefined || $rootScope.GettingStartedemailId == null) {
        $rootScope.GettingStartedemailId = "Enter your email...";
      }
    }

    $rootScope.clearGSEmail = function() {
      $rootScope.GettingStartedemailId = "";
    }

  var sendData = function(type, URL, formData, callBack){
    var xhr = new XMLHttpRequest();
    xhr.open(type, URL, true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=ISO-8859-1')
    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4 && xhr.status == 200) {
        var response = xhr.responseText;
        callBack(response);
      }
    };
    if(type == "GET"){
      xhr.send();
    }
    if(type == "POST"){
      xhr.send(formData);
    }
  };


  $rootScope.GettingStartedClicked = function(email) {
    var emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    

    if($rootScope.GettingStartedBtnTxt == "Request Early Access") {
      if(!emailRegx.test(email)) {
            utilityService.serviceError("Please Enter Valid Email Address");
            return;
          }

        $rootScope.myValue = "move";
        $rootScope.GettingStartedBtnTxt = "";
        $timeout(function() {
          $rootScope.GettingStartedBtnTxt = "Thank You"
        }, 2000); 

        var url = "./lib/send-mail.php";

          var from = email + "<" + email + ">";
          var to = "TO NAME <amal@pencilanderaser.in>";
          var sub = "PnE subject";
          var msg = 'Contact Us';
          var formdata = "from="+from+"&to="+to+"&sub="+sub+"&msg="+msg+"";

          sendData("POST", url, formdata, function(r){
            console.log(r);
          });
    }
    
  }

    


});

angular.module('keitoApp')
  .controller('pricingCtrl', function ($rootScope,$scope,$location,$anchorScroll,$uibModal,$log,utilityService,focus,$timeout) {
    

    $scope.ContactEmailID = "you@domain.com (mandatory)";
    $scope.ContactDescription = "Type here..";

    $scope.othersSubject = "subject (helpful but optional)";

    $scope.outOfInputFocusothersSubject = function() {
      if($scope.othersSubject == '' || $scope.othersSubject == undefined || $scope.othersSubject == null) {
        $scope.othersSubject = "subject (helpful but optional)";
      }
    }

    $scope.outOfInputFocus = function() {
      if($scope.ContactEmailID == '' || $scope.ContactEmailID == undefined || $scope.ContactEmailID == null) {
        $scope.ContactEmailID = "you@domain.com (mandatory)";
      }
    }

    $scope.outOfDesFocus =  function() {
      if($scope.ContactDescription == '' || $scope.ContactDescription == undefined || $scope.ContactDescription == null) {
        $scope.ContactDescription = "Type Here..";
      }
    }
    
    $scope.sendContactUsDetails = function() {

       var emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    
      if(!emailRegx.test($scope.ContactEmailID)) {
        utilityService.serviceError("Please Enter Valid Email Address");
        return;
      }

      var sub = $scope.subject;
      var industry = $scope.industry;
      var noOfEmployees = $scope.noOfEmployees;
      var ContactDescription = $scope.ContactDescription;

      if($scope.subject == "others") {
        sub = $scope.othersSubject;
      }

      if($scope.industry == '#') {
        industry = '';
      }

       if($scope.ContactDescription == '#') {
        sContactDescriptionub = '';
      }

       if($scope.noOfEmployees == '#') {
        noOfEmployees = '';
      }

       if($scope.subject == '#') {
        sub = '';
      }

         

          var url = "./lib/send-mail.php";

          var from = $scope.ContactEmailID + "<" + $scope.ContactEmailID + ">";
          var to = "TO NAME <amal@pencilanderaser.in>";
          var msg = 'Industry - '+industry +'\nno Of Employees -'+noOfEmployees+'\nContact Description - '+ContactDescription;



          var formdata = "from="+from+"&to="+to+"&sub="+sub+"&msg="+msg+"";

          $rootScope.progressBarText1 = "Collecting Details";
            $rootScope.progressBarText2 = "Updating Details";
            $rootScope.progressBarText3 = "Sending Mail";
            $rootScope.progressBarShow = true;

            sendData("POST", url, formdata, function(r){
            console.log(r);

            $rootScope.progressBarShow = false;
            

            $scope.ContactEmailID = undefined;
            $scope.subject = "#";
            $scope.industry =  '#';
            $scope.noOfEmployees = '#';
            $scope.ContactDescription = 'Type Here..';

          });
    }


    $rootScope.GettingStartedemailId = "Enter your email...";
    
    
    $rootScope.GettingStartedBtnTxt = "Request Early Access";

  $rootScope.GettingStartedemailIdFocus = function() {
    $rootScope.GettingStartedemailId = "";
    focus('focusMe'); 
  }

  $rootScope.outOfGettingStartedFocus = function() {
      if($rootScope.GettingStartedemailId == "" || $rootScope.GettingStartedemailId == undefined || $rootScope.GettingStartedemailId == null) {
        $rootScope.GettingStartedemailId = "Enter your email...";
      }
    }

    $rootScope.clearGSEmail = function() {
      $rootScope.GettingStartedemailId = "";
    }

  var sendData = function(type, URL, formData, callBack){
    var xhr = new XMLHttpRequest();
    xhr.open(type, URL, true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=ISO-8859-1')
    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4 && xhr.status == 200) {
        var response = xhr.responseText;
        callBack(response);
      }
    };
    if(type == "GET"){
      xhr.send();
    }
    if(type == "POST"){
      xhr.send(formData);
      $rootScope.progressBarShow = false;
    }
  };


  $rootScope.GettingStartedClicked = function(email) {
    var emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    

    if($rootScope.GettingStartedBtnTxt == "Request Early Access") {
      if(!emailRegx.test(email)) {
            utilityService.serviceError("Please Enter Valid Email Address");
            return;
          }

        $rootScope.myValue = "move";
        $rootScope.GettingStartedBtnTxt = "";
        $timeout(function() {
          $rootScope.GettingStartedBtnTxt = "Thank You"
        }, 2000); 

        var url = "./lib/send-mail.php";

          var from = email + "<" + email + ">";
          var to = "TO NAME <amal@pencilanderaser.in>";
          var sub = "PnE subject";
          var msg = 'Contact Us';
          var formdata = "from="+from+"&to="+to+"&sub="+sub+"&msg="+msg+"";

          sendData("POST", url, formdata, function(r){
            console.log(r);
          });

    }
    
  }

    

    $(".navbar").removeClass("addHeaderActiveClassWhite");
    $(".navbar").removeClass("addHeaderActiveClass");

    $(".navbar").addClass("addHeaderDeactiveClass");

    $(function() {
      $(window).on("scroll", function() {
          if($(window).scrollTop() > 50) {
            $(".navbar").removeClass("addHeaderActiveClass");
            $(".navbar").removeClass("addHeaderDeactiveClass");
              $(".navbar").addClass("addHeaderActiveClassWhite");
          } else {
              //remove the background property so it comes transparent again (defined in your css)
             $(".navbar").addClass("addHeaderDeactiveClass");
             $(".navbar").removeClass("addHeaderActiveClassWhite");
          }
      });
  });

    $rootScope.mbody = "pbody";
    $rootScope.footerclr = "";
    $rootScope.footerhide = false;
    $scope.prev;

    document.getElementById('afterLoading').style.display = "";
    document.getElementById('progressBarShowLoading').style.display = "none";

    $rootScope.progressBarShow = false;
    $rootScope.progressBarShowLoading = false;

    $scope.priceActiveBas = "pricingDeactive";
    $scope.priceActiveValBas = "pricingDeactiveclr";
    $scope.actvPriceBtnBas = "dactvPriceBtn";
    $scope.basicTxtclr = "";

    $scope.priceActivePre ="pricingDeactive";
    $scope.priceActiveValPre ="pricingDeactiveclr";
    $scope.actvPriceBtnPre ="dactvPriceBtn";
    $scope.preTxtclr = "";

    $scope.priceBasSelect = function() {
        $scope.priceActiveBas = "pricingActive";
        $scope.priceActiveValBas = "pricingActiveclr";
        $scope.actvPriceBtnBas = "actvPriceBtn";
        $scope.basicTxtclr = "txtclr";

        $scope.priceActivePre ="pricingDeactive";
        $scope.priceActiveValPre ="pricingDeactiveclr";
        $scope.actvPriceBtnPre ="dactvPriceBtn";
        $scope.preTxtclr = "";
    }

    $scope.pricePreSelect = function() {
        $scope.priceActiveBas  ="pricingDeactive";                
        $scope.priceActiveValBas ="pricingDeactiveclr";
        $scope.actvPriceBtnBas ="dactvPriceBtn";
        $scope.basicTxtclr = "";

        $scope.priceActivePre = "pricingActive";
        $scope.priceActiveValPre = "pricingActiveclr";
        $scope.actvPriceBtnPre = "actvPriceBtn";
        $scope.preTxtclr = "txtclr";
    }

  });

angular.module('keitoApp')
  .controller('industriesCtrl', function ($rootScope,$scope,$location,$anchorScroll,$uibModal,$log,utilityService,focus,$timeout) {

    $rootScope.GettingStartedemailId = "Enter your email...";
    
    
    $rootScope.GettingStartedBtnTxt = "Request Early Access";

  $rootScope.GettingStartedemailIdFocus = function() {
    $rootScope.GettingStartedemailId = "";
    focus('focusMe'); 
  }

  $rootScope.outOfGettingStartedFocus = function() {
      if($rootScope.GettingStartedemailId == "" || $rootScope.GettingStartedemailId == undefined || $rootScope.GettingStartedemailId == null) {
        $rootScope.GettingStartedemailId = "Enter your email...";
      }
    }

    $rootScope.clearGSEmail = function() {
      $rootScope.GettingStartedemailId = "";
    }

  var sendData = function(type, URL, formData, callBack){
    var xhr = new XMLHttpRequest();
    xhr.open(type, URL, true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=ISO-8859-1')
    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4 && xhr.status == 200) {
        var response = xhr.responseText;
        callBack(response);
      }
    };
    if(type == "GET"){
      xhr.send();
    }
    if(type == "POST"){
      xhr.send(formData);
    }
  };


  $rootScope.GettingStartedClicked = function(email) {
    var emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    

    if($rootScope.GettingStartedBtnTxt == "Request Early Access") {
      if(!emailRegx.test(email)) {
            utilityService.serviceError("Please Enter Valid Email Address");
            return;
          }

        $rootScope.myValue = "move";
        $rootScope.GettingStartedBtnTxt = "";
        $timeout(function() {
          $rootScope.GettingStartedBtnTxt = "Thank You"
        }, 2000); 

        var url = "./lib/send-mail.php";

          var from = email + "<" + email + ">";
          var to = "TO NAME <amal@pencilanderaser.in>";
          var sub = "PnE subject";
          var msg = 'Contact Us';
          var formdata = "from="+from+"&to="+to+"&sub="+sub+"&msg="+msg+"";

          sendData("POST", url, formdata, function(r){
            console.log(r);
          });
    }
    
  }


    $(".navbar").removeClass("addHeaderActiveClassWhite");
    $(".navbar").removeClass("addHeaderActiveClass");

    $(".navbar").addClass("addHeaderDeactiveClass");

    $(function() {
      $(window).on("scroll", function() {
          if($(window).scrollTop() > 50) {
            $(".navbar").removeClass("addHeaderActiveClassWhite");
            $(".navbar").removeClass("addHeaderDeactiveClass");
              $(".navbar").addClass("addHeaderActiveClass");
          } else {
              //remove the background property so it comes transparent again (defined in your css)
             $(".navbar").addClass("addHeaderDeactiveClass");
             $(".navbar").removeClass("addHeaderActiveClass");
          }
      });
  });

    $rootScope.subscribe = function () {
      //$rootScope.prevMbody = $rootScope.mbody
      

      var modalInstance = $uibModal.open({
        //animation: $scope.animationsEnabled,
        templateUrl: 'views/modals/subscribe.html',
        controller: 'ModalSubscribeCtrl'
      });
      $rootScope.blurClass = "blurmbody";
      modalInstance.result.then(function (selectedItem) {
        $log.info('Modal dismissed at: ' + new Date());
        $rootScope.blurClass = "";
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
        $rootScope.blurClass = "";
      });
  };


    $rootScope.mbody = "pbody";
    $rootScope.footerclr = "";
    $rootScope.footerhide = false;
    $scope.prev;

    document.getElementById('afterLoading').style.display = "";
    document.getElementById('progressBarShowLoading').style.display = "none";

    $rootScope.progressBarShow = false;
    $rootScope.progressBarShowLoading = false;
    
  $scope.gotodiv = function(newHash) {
      if ($location.hash() !== newHash) {
        $location.hash(newHash);
      } else {
        $anchorScroll();
      }
    };

  $scope.displayImage = function(val)  {

      $scope[$scope.prev] = "" 

      switch(val) {
        case 1: $scope.dactive = "divActive"
            $scope.prev = "dactive";
            break;
        case 2: $scope.cactive = "divActive";
            $scope.prev = "cactive";
            break;
        case 3: $scope.ctactive = "divActive"
            $scope.prev = "ctactive";
            break;
        case 4: $scope.sactive = "divActive"
            $scope.prev = "sactive";
            break;
        case 5: $scope.kactive = "divActive"
            $scope.prev = "kactive";
            break;

      }
    }

    

  });


angular.module('keitoApp')
  .controller('myProfileCtrl', function ($rootScope,$scope,$uibModal,$log,utilityService,signInService,Users) {
        
        $scope.deleteChatVal = true;
        $scope.largeFormat = true;
        $scope.status = "Available";

        $scope.statusFormat = false;
        $scope.passwordFormat = false;
        $scope.showProfilePicture = Users.getShowProfilePicture($rootScope.loggedinuser.id);

        $scope.profilePictureViscibility = function () {
          if(!Users.getProfile($rootScope.loggedinuser.id)) {
            $scope.showProfilePicture = false;
            utilityService.serviceError("Please Upload Profile Picture.");
            return;
          }

          var obj = {
            "showProfilePicture" : $scope.showProfilePicture
          }
          signInService.updateUser(obj,$rootScope.AUTHTOKEN).then(function(response) {
            $rootScope.progressBarShow = false;
            utilityService.serviceError("Profile setting updated successfully.");
            })

        }
        $scope.setStatus = function(status) {
          console.log(status);
          if(!$scope.statusFormat){
            $scope.largeFormat = false;
            $scope.statusFormat = true;
            $scope.passwordFormat = false;
          }
          else {
            $scope.largeFormat = true;
            $scope.statusFormat = false;
            $scope.passwordFormat = false;
          }
        }

        $scope.changePassword = function() {
          if(!$scope.passwordFormat) {
            $scope.largeFormat = false;
            $scope.statusFormat = false;
            $scope.passwordFormat = true;
          }
          else {
            $scope.largeFormat = true;
            $scope.statusFormat = false;
            $scope.passwordFormat = false;
          }
        }

        $scope.changeEmailPassword = function(currentPassword,newPassword,confirmNewPassword) {
         // utilityService.serviceError('Email verification through mail is not working');
          if(newPassword.length <5) {
            utilityService.serviceError("New Password length should be atleast 5 characters");
            return;
          }

          if(newPassword != confirmNewPassword) {
            utilityService.serviceError("New Password and Confirmation password didnt not match.");
            return;
          }

          var obj  = {
                       "password" : currentPassword,
                       "newPassword" : newPassword,
                       "newPasswordConfirmation" : confirmNewPassword
              }


           $rootScope.progressBarText1 = "Fecthing Password";
          $rootScope.progressBarText2 = "Validating Password";
          $rootScope.progressBarText3 = "Updating Password";
          $rootScope.progressBarShow = true;


          signInService.updatePassword(obj,$rootScope.AUTHTOKEN).then(function(response) {
            $rootScope.progressBarShow = false;
            utilityService.serviceError("Password changed successfully");
            $scope.largeFormat = true;
            $scope.statusFormat = false;
            $scope.passwordFormat = false;

            $scope.currentPassword = '';
            $scope.newPassword = '';
            $scope.confirmNewPassword = '';
          })

          
        }


        $scope.setStatusDone = function(status) {
          $scope.largeFormat = true;
          $scope.statusFormat = false;
          $scope.passwordFormat = false;
          $scope.status = status;
          console.log(status);

          var obj = {
            "status" : status
          }
          signInService.updateUser(obj,$rootScope.AUTHTOKEN).then(function(response) {
            $rootScope.progressBarShow = false;
            utilityService.serviceError("Status updated successfully.");
            })

        }

        $scope.resendVerificationMail = function(email) {
          console.log(email);
          var obj = {
            "email" : email
          }

          $rootScope.progressBarText1 = "Fecthing Mail Id";
          $rootScope.progressBarText2 = "Validating Mail Id";
          $rootScope.progressBarText3 = "Sending Mail Confirmation";
          $rootScope.progressBarShow = true;

          signInService.resendMailConfirmation(obj).then(function(response) {
            $rootScope.progressBarShow = false;
            utilityService.serviceError("Verification mail sent successfully.");
             
            
          })


        }

        $scope.deleteChat = function() {
          console.log($scope.deleteChatVal);

          if(!$scope.deleteChatVal) {
            var modalInstance = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'views/modals/deleteChatsModal.html',
              controller: 'deleteChatsModalCtrl'
            });

            modalInstance.result.then(function (selectedItem) {
              utilityService.serviceError(selectedItem);
            }, function () {
              $log.info('Modal dismissed at: ' + new Date());
            });
          } 
        }

        $scope.Users = Users;
        $scope.uploadFile = function(files) {
            var file    = files[0];
            var reader  = new FileReader();

            var ImageRef = firebase.database().ref('ProfileImages').child($rootScope.loggedinuser.id);

            var storageRef = firebase.storage().ref();

            // Create a reference to 'mountains.jpg'
            //var mountainsRef = storageRef.child('Profile_Images');
//
            // Create a reference to 'images/mountains.jpg'
            //var mountainImagesRef = storageRef.child('Profile_Images/mountains.jpg');


            reader.addEventListener("load", function () {
              //preview.src = reader.result;
              ImageRef.set({
                'image': reader.result
              }, function(snap) {
                    var obj = {
                      "showProfilePicture" : true
                    }
                    signInService.updateUser(obj,$rootScope.AUTHTOKEN).then(function(response) {
                      $rootScope.progressBarShow = false;
                      utilityService.serviceError("profile picture updated successfully.");
                      })
                    //utilityService.serviceError(" updated succesfully")
                  })

              //mountainImagesRef.putString(reader.result, 'data_url').then(function(snapshot) {
              //  console.log('Uploaded a base64url string!');
              //});

            });


            
            reader.readAsDataURL(file);
            
        }   

  });


  

  angular.module('keitoApp').controller('deleteChatsModalCtrl', function ($uibModalInstance,$scope) {
  //var $ctrl = this;
      
      $scope.ok = function () {
        $uibModalInstance.close("ok");
      };

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
});


angular.module('keitoApp').controller('alertCtrl', function ($uibModalInstance,$scope,msg) {
  $scope.msg = msg;
  $scope.ok = function () {
    $uibModalInstance.dismiss('cancel');
  };
});


angular.module('keitoApp').controller('confirmCtrl', function ($uibModalInstance,$scope,msg) {
  $scope.msg = msg;
  $scope.ok = function () {
    $uibModalInstance.dismiss(true);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss(false);
  };
});


angular.module('keitoApp').controller('descriptionCtrl', function ($uibModalInstance,$scope,msg) {
  $scope.msg = msg;
  $scope.ok = function () {
    $uibModalInstance.dismiss(true);
  };
});

angular.module('keitoApp').directive('contenteditable', function() {
    return {
      restrict: 'A', // only activate on element attribute
      require: '?ngModel', // get a hold of NgModelController
      link: function(scope, element, attrs, ngModel) {
        if(!ngModel) return; // do nothing if no ng-model

        // Specify how UI should be updated
        ngModel.$render = function() {
          element.html(ngModel.$viewValue || '');
        };

        // Listen for change events to enable binding
        element.on('blur keyup change', function() {
          scope.$apply(read);
        });
        read(); // initialize

        // Write data to the model
        function read() {
          var html = element.html();
          // When we clear the content editable the browser leaves a <br> behind
          // If strip-br attribute is provided then we strip this out
          if( attrs.stripBr && html == '<br>' ) {
            html = '';
          }
          ngModel.$setViewValue(html);
        }
      }
    };
  });


angular.module('keitoApp').directive('focusOn', function() {
   return function(scope, elem, attr) {
      scope.$on(attr.focusOn, function(e) {
          elem[0].focus();
      });
   };
});