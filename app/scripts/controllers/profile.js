angular.module('keitoApp')
  .controller('profileCtrl', function ($rootScope, $scope, $uibModal, $log, $document, $state, createTeamService, discussionService, utilityService, chatService, $firebaseArray, Users) {

    function init() {
      if (typeof jQuery === "undefined") {
        throw new Error("jQuery plugins need to be before this file");
      }

      $.AdminBSB = {};
      $.AdminBSB.options = {
        colors: {
          red: '#F44336',
          pink: '#E91E63',
          purple: '#9C27B0',
          deepPurple: '#673AB7',
          indigo: '#3F51B5',
          blue: '#2196F3',
          lightBlue: '#03A9F4',
          cyan: '#00BCD4',
          teal: '#009688',
          green: '#4CAF50',
          lightGreen: '#8BC34A',
          lime: '#CDDC39',
          yellow: '#ffe821',
          amber: '#FFC107',
          orange: '#FF9800',
          deepOrange: '#FF5722',
          brown: '#795548',
          grey: '#9E9E9E',
          blueGrey: '#607D8B',
          black: '#000000',
          white: '#ffffff'
        },
        leftSideBar: {
          scrollColor: 'rgba(0,0,0,0.5)',
          scrollWidth: '4px',
          scrollAlwaysVisible: false,
          scrollBorderRadius: '0',
          scrollRailBorderRadius: '0',
          scrollActiveItemWhenPageLoad: true,
          breakpointWidth: 1170
        },
        dropdownMenu: {
          effectIn: 'fadeIn',
          effectOut: 'fadeOut'
        }
      }

      /* Left Sidebar - Function =================================================================================================
      *  You can manage the left sidebar menu options
      *  
      */
      $.AdminBSB.leftSideBar = {
        activate: function () {
          var _this = this;
          var $body = $('body');
          var $overlay = $('.overlay');

          //Close sidebar
          $(window).click(function (e) {
            var $target = $(e.target);
            if (e.target.nodeName.toLowerCase() === 'i') { $target = $(e.target).parent(); }

            if (!$target.hasClass('bars') && _this.isOpen() && $target.parents('#leftsidebar').length === 0) {
              if (!$target.hasClass('js-right-sidebar')) $overlay.fadeOut();
              $body.removeClass('overlay-open');
            }
          });

          $.each($('.menu-toggle.toggled'), function (i, val) {
            $(val).next().slideToggle(0);
          });

          //When page load
          $.each($('.menu .list li.active'), function (i, val) {
            var $activeAnchors = $(val).find('a:eq(0)');

            $activeAnchors.addClass('toggled');
            $activeAnchors.next().show();
          });

          //Collapse or Expand Menu
          $('.menu-toggle').on('click', function (e) {
            var $this = $(this);
            var $content = $this.next();

            if ($($this.parents('ul')[0]).hasClass('list')) {
              var $not = $(e.target).hasClass('menu-toggle') ? e.target : $(e.target).parents('.menu-toggle');

              $.each($('.menu-toggle.toggled').not($not).next(), function (i, val) {
                if ($(val).is(':visible')) {
                  $(val).prev().toggleClass('toggled');
                  $(val).slideUp();
                }
              });
            }

            $this.toggleClass('toggled');
            $content.slideToggle(320);
          });

          //Set menu height
          _this.setMenuHeight();
          _this.checkStatuForResize(true);
          $(window).resize(function () {
            _this.setMenuHeight();
            _this.checkStatuForResize(false);
          });

          //Set Waves
          Waves.attach('.menu .list a', ['waves-block']);
          Waves.init();
        },
        setMenuHeight: function (isFirstTime) {
          if (typeof $.fn.slimScroll != 'undefined') {
            var configs = $.AdminBSB.options.leftSideBar;
            var height = ($(window).height() - ($('.legal').outerHeight() + $('.user-info').outerHeight() + $('.navbar').innerHeight()));
            var $el = $('.list');

            $el.slimscroll({
              height: height + "px",
              color: configs.scrollColor,
              size: configs.scrollWidth,
              alwaysVisible: configs.scrollAlwaysVisible,
              borderRadius: configs.scrollBorderRadius,
              railBorderRadius: configs.scrollRailBorderRadius
            });

            //Scroll active menu item when page load, if option set = true
            if ($.AdminBSB.options.leftSideBar.scrollActiveItemWhenPageLoad) {
              var activeItemOffsetTop = $('.menu .list li.active')[0].offsetTop
              if (activeItemOffsetTop > 150) $el.slimscroll({ scrollTo: activeItemOffsetTop + 'px' });
            }
          }
        },
        checkStatuForResize: function (firstTime) {
          var $body = $('body');
          var $openCloseBar = $('.navbar .navbar-header .bars');
          var width = $body.width();

          if (firstTime) {
            $body.find('.content, .sidebar').addClass('no-animate').delay(1000).queue(function () {
              $(this).removeClass('no-animate').dequeue();
            });
          }

          if (width < $.AdminBSB.options.leftSideBar.breakpointWidth) {
            $body.addClass('ls-closed');
            $openCloseBar.fadeIn();
          }
          else {
            $body.removeClass('ls-closed');
            $openCloseBar.fadeOut();
          }
        },
        isOpen: function () {
          return $('body').hasClass('overlay-open');
        }
      };
      //==========================================================================================================================

      /* Right Sidebar - Function ================================================================================================
      *  You can manage the right sidebar menu options
      *  
      */
      $.AdminBSB.rightSideBar = {
        activate: function () {
          var _this = this;
          var $sidebar = $('#rightsidebar');
          var $overlay = $('.overlay');

          //Close sidebar
          $(window).click(function (e) {
            var $target = $(e.target);
            if (e.target.nodeName.toLowerCase() === 'i') { $target = $(e.target).parent(); }

            if (!$target.hasClass('js-right-sidebar') && _this.isOpen() && $target.parents('#rightsidebar').length === 0) {
              if (!$target.hasClass('bars')) $overlay.fadeOut();
              $sidebar.removeClass('open');
            }
          });

          $('.js-right-sidebar').on('click', function () {
            $sidebar.toggleClass('open');
            if (_this.isOpen()) { $overlay.fadeIn(); } else { $overlay.fadeOut(); }
          });
        },
        isOpen: function () {
          return $('.right-sidebar').hasClass('open');
        }
      }

      var $searchBar = $('.search-bar');
      $.AdminBSB.search = {
        activate: function () {
          var _this = this;

          //Search button click event
          $('.js-search').on('click', function () {
            _this.showSearchBar();
          });

          //Close search click event
          $searchBar.find('.close-search').on('click', function () {
            _this.hideSearchBar();
          });

          //ESC key on pressed
          $searchBar.find('input[type="text"]').on('keyup', function (e) {
            if (e.keyCode == 27) {
              _this.hideSearchBar();
            }
          });
        },
        showSearchBar: function () {
          $searchBar.addClass('open');
          $searchBar.find('input[type="text"]').focus();
        },
        hideSearchBar: function () {
          $searchBar.removeClass('open');
          $searchBar.find('input[type="text"]').val('');
        }
      }
      //==========================================================================================================================

      /* Navbar - Function =======================================================================================================
      *  You can manage the navbar
      *  
      */
      $.AdminBSB.navbar = {
        activate: function () {
          var $body = $('body');
          var $overlay = $('.overlay');

          //Open left sidebar panel
          $('.bars').on('click', function () {
            $body.toggleClass('overlay-open');
            if ($body.hasClass('overlay-open')) { $overlay.fadeIn(); } else { $overlay.fadeOut(); }
          });

          //Close collapse bar on click event
          $('.nav [data-close="true"]').on('click', function () {
            var isVisible = $('.navbar-toggle').is(':visible');
            var $navbarCollapse = $('.navbar-collapse');

            if (isVisible) {
              $navbarCollapse.slideUp(function () {
                $navbarCollapse.removeClass('in').removeAttr('style');
              });
            }
          });
        }
      }
      //==========================================================================================================================

      /* Input - Function ========================================================================================================
      *  You can manage the inputs(also textareas) with name of class 'form-control'
      *  
      */
      $.AdminBSB.input = {
        activate: function () {
          //On focus event
          $('.form-control').focus(function () {
            $(this).parent().addClass('focused');
          });

          //On focusout event
          $('.form-control').focusout(function () {
            var $this = $(this);
            if ($this.parents('.form-group').hasClass('form-float')) {
              if ($this.val() == '') { $this.parents('.form-line').removeClass('focused'); }
            }
            else {
              $this.parents('.form-line').removeClass('focused');
            }
          });

          //On label click
          $('body').on('click', '.form-float .form-line .form-label', function () {
            $(this).parent().find('input').focus();
          });

          //Not blank form
          $('.form-control').each(function () {
            if ($(this).val() !== '') {
              $(this).parents('.form-line').addClass('focused');
            }
          });
        }
      }
      //==========================================================================================================================

      /* Form - Select - Function ================================================================================================
      *  You can manage the 'select' of form elements
      *  
      */
      $.AdminBSB.select = {
        activate: function () {
          if ($.fn.selectpicker) { $('select:not(.ms)').selectpicker(); }
        }
      }
      //==========================================================================================================================

      /* DropdownMenu - Function =================================================================================================
      *  You can manage the dropdown menu
      *  
      */

      $.AdminBSB.dropdownMenu = {
        activate: function () {
          var _this = this;

          $('.dropdown, .dropup, .btn-group').on({
            "show.bs.dropdown": function () {
              var dropdown = _this.dropdownEffect(this);
              _this.dropdownEffectStart(dropdown, dropdown.effectIn);
            },
            "shown.bs.dropdown": function () {
              var dropdown = _this.dropdownEffect(this);
              if (dropdown.effectIn && dropdown.effectOut) {
                _this.dropdownEffectEnd(dropdown, function () { });
              }
            },
            "hide.bs.dropdown": function (e) {
              var dropdown = _this.dropdownEffect(this);
              if (dropdown.effectOut) {
                e.preventDefault();
                _this.dropdownEffectStart(dropdown, dropdown.effectOut);
                _this.dropdownEffectEnd(dropdown, function () {
                  dropdown.dropdown.removeClass('open');
                });
              }
            }
          });

          //Set Waves
          Waves.attach('.dropdown-menu li a', ['waves-block']);
          Waves.init();
        },
        dropdownEffect: function (target) {
          var effectIn = $.AdminBSB.options.dropdownMenu.effectIn, effectOut = $.AdminBSB.options.dropdownMenu.effectOut;
          var dropdown = $(target), dropdownMenu = $('.dropdown-menu', target);

          if (dropdown.length > 0) {
            var udEffectIn = dropdown.data('effect-in');
            var udEffectOut = dropdown.data('effect-out');
            if (udEffectIn !== undefined) { effectIn = udEffectIn; }
            if (udEffectOut !== undefined) { effectOut = udEffectOut; }
          }

          return {
            target: target,
            dropdown: dropdown,
            dropdownMenu: dropdownMenu,
            effectIn: effectIn,
            effectOut: effectOut
          };
        },
        dropdownEffectStart: function (data, effectToStart) {
          if (effectToStart) {
            data.dropdown.addClass('dropdown-animating');
            data.dropdownMenu.addClass('animated dropdown-animated');
            data.dropdownMenu.addClass(effectToStart);
          }
        },
        dropdownEffectEnd: function (data, callback) {
          var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
          data.dropdown.one(animationEnd, function () {
            data.dropdown.removeClass('dropdown-animating');
            data.dropdownMenu.removeClass('animated dropdown-animated');
            data.dropdownMenu.removeClass(data.effectIn);
            data.dropdownMenu.removeClass(data.effectOut);

            if (typeof callback == 'function') {
              callback();
            }
          });
        }
      }
      //==========================================================================================================================

      /* Browser - Function ======================================================================================================
      *  You can manage browser
      *  
      */
      var edge = 'Microsoft Edge';
      var ie10 = 'Internet Explorer 10';
      var ie11 = 'Internet Explorer 11';
      var opera = 'Opera';
      var firefox = 'Mozilla Firefox';
      var chrome = 'Google Chrome';
      var safari = 'Safari';

      $.AdminBSB.browser = {
        activate: function () {
          var _this = this;
          var className = _this.getClassName();

          if (className !== '') $('html').addClass(_this.getClassName());
        },
        getBrowser: function () {
          var userAgent = navigator.userAgent.toLowerCase();

          if (/edge/i.test(userAgent)) {
            return edge;
          } else if (/rv:11/i.test(userAgent)) {
            return ie11;
          } else if (/msie 10/i.test(userAgent)) {
            return ie10;
          } else if (/opr/i.test(userAgent)) {
            return opera;
          } else if (/chrome/i.test(userAgent)) {
            return chrome;
          } else if (/firefox/i.test(userAgent)) {
            return firefox;
          } else if (!!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/)) {
            return safari;
          }

          return undefined;
        },
        getClassName: function () {
          var browser = this.getBrowser();

          if (browser === edge) {
            return 'edge';
          } else if (browser === ie11) {
            return 'ie11';
          } else if (browser === ie10) {
            return 'ie10';
          } else if (browser === opera) {
            return 'opera';
          } else if (browser === chrome) {
            return 'chrome';
          } else if (browser === firefox) {
            return 'firefox';
          } else if (browser === safari) {
            return 'safari';
          } else {
            return '';
          }
        }
      }
      //==========================================================================================================================

      $(function () {
        $.AdminBSB.browser.activate();
        $.AdminBSB.leftSideBar.activate();
        $.AdminBSB.rightSideBar.activate();
        $.AdminBSB.navbar.activate();
        $.AdminBSB.dropdownMenu.activate();
        $.AdminBSB.input.activate();
        $.AdminBSB.select.activate();
        $.AdminBSB.search.activate();

        setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
      });
    }

    
    init();

    $scope.Users = Users;

    $rootScope.unreadCount = 0;

    $rootScope.teamUnread = 0;

    $scope.curMenu = "kbox";
    $scope.sideMenuClick = function(value) {
       //$state.go($state.current, {}, { reload: true });

      $scope.viewItem = value;
    }
    
    $scope.setTeamId = function(id) {
      $state.go("profile.team.chats", {"id":id}, { reload: true });

    }
    
    $scope.backToDiscussion = function () {
      $scope.changeCreateDiscussion('teams');
      $state.go('profile.productdiscussions');
    }

    $scope.backToKboxDiscussion = function () {
      $scope.changeCreateDiscussion('kbox');
      $state.go('profile.discussions');
    };

    $scope.backToChats = function () {
      $scope.changeCreateDiscussion('teams');
      $state.go('profile.productchats');
    }

    $scope.backToKboxChats = function () {
      $scope.changeCreateDiscussion('kbox');
      $state.go('profile.chats');
    }

    $rootScope.getDomainName = function (str) {
      var domain = str.split("@");
      return domain[1];
    }

    $scope.gotoTeam = function () {
      $state.go("profile.productchats", {}, { reload: true })
    }

    $scope.showsmallMenu = false;

    $scope.changeMenuFormat = function () {
      $scope.showsmallMenu = !$scope.showsmallMenu;
    }

    $rootScope.updateUnreadMessagesForTeam = function () {
      chatService.getChatsByTeamId($rootScope.currentTeam, $rootScope.AUTHTOKEN).then(function (response) {
        var teamChats = [];
        teamChats = response.INDIVIDUAL;

        $rootScope.teamUnread = 0;
        if (teamChats != undefined) {
          for (var i = 0; i < teamChats.length; i++) {
            for (var j = 0; j < teamChats[i].members.length; j++) {
              if (teamChats[i].members[j].userId != $rootScope.loggedinuser.id) {
                $rootScope.teamUnread = $rootScope.teamUnread + chatService.userDetailsRef.$getRecord($rootScope.loggedinuser.id).INDIVIDUAL_CHATS[teamChats[i].members[j].userId].unread_msg_count;
              }
            }
          }
        }
      });
    }

    $rootScope.updateUnreadMessagesForTeamDiscussion = function () {

      chatService.getChatsByTeamId($rootScope.currentTeam, $rootScope.AUTHTOKEN).then(function (response) {

        $rootScope.teamUnreadDiscussions = 0;

        teamDiscussion = response.DISCUSSION;
        teamGroup = response.GROUP;

        if (teamDiscussion != undefined) {
          for (var i = 0; i < teamDiscussion.length; i++) {
            $rootScope.teamUnreadDiscussions = $rootScope.teamUnreadDiscussions + chatService.userDetailsRef.$getRecord($rootScope.loggedinuser.id).DISCUSSIONS[teamDiscussion[i].id].unread_msg_count;
          }
        }

        if (teamDiscussion != undefined) {
          for (var i = 0; i < teamGroup.length; i++) {
            $rootScope.teamUnreadDiscussions = $rootScope.teamUnreadDiscussions + chatService.userDetailsRef.$getRecord($rootScope.loggedinuser.id).GROUP_CHATS[teamGroup[i].id].unread_msg_count;
          }
        }


      });


    };

    $rootScope.updateUnreadMessagesInkbox = function () {
      chatService.getAllChats($scope.loggedinuser.id).$loaded().then(function (allChatUsers) {

        chatService.userDetailsRef.$loaded().then(function (Message) {

          $scope.chatUsers = allChatUsers;
          $rootScope.unreadCount = 0;

          for (var i = 0; i < allChatUsers.length; i++) {
            $rootScope.unreadCount = $rootScope.unreadCount + chatService.userDetailsRef.$getRecord($scope.loggedinuser.id).INDIVIDUAL_CHATS[allChatUsers[i].$id].unread_msg_count;
          }

          discussionService.getAllGroupChats($scope.loggedinuser.id).$loaded().then(function (groupChats) {
            discussionService.getAllDiscussions($scope.loggedinuser.id).$loaded().then(function (discussions) {
              $rootScope.progressBarShow = false;
              $scope.allDiscussionUsers = [];
              $scope.allDiscussionUsers = groupChats;
              $scope.allDiscussionUsers.push.apply(groupChats, discussions);
              for (var j = 0; j < $scope.allDiscussionUsers.length; j++) {
                if ($scope.allDiscussionUsers[j].chat_type == 'GROUP') {
                  $rootScope.unreadCount = $rootScope.unreadCount + chatService.userDetailsRef.$getRecord($scope.loggedinuser.id).GROUP_CHATS[$scope.allDiscussionUsers[j].$id].unread_msg_count;
                }
                else {
                  $rootScope.unreadCount = $rootScope.unreadCount + chatService.userDetailsRef.$getRecord($scope.loggedinuser.id).DISCUSSIONS[$scope.allDiscussionUsers[j].$id].unread_msg_count;
                }

              }
            });

          });



        });

      });
    };
    $rootScope.updateUnreadMessagesInkbox();

    $rootScope.mbody = "prbody";
    $rootScope.footerclr = "";
    $rootScope.footerhide = true;
    $scope.currentProfile = 'chats';
    $scope.viewItem = "kbox";

    switch ($state.current.url) {
      case "/createteam": $scope.viewItem = "createteam"; break;

      case "/chats": $scope.viewItem = "kbox"; break;

      case "/productchats": $scope.viewItem = "teams"; break;
    }

    $scope.setViewItem = function (item) {
      if ($scope.viewItem == "createteam" && item == "createteam") {
        $scope.viewItem = item;
        $state.go($state.current, {}, { reload: true });
        return;
      }

      $scope.viewItem = item;
      $scope.activeTab(1);
      $scope.producttab('Chats');
    }

    $scope.setViewSettingSViewItem = function (item) {
      $scope.viewItem = item;

    }

    $scope.hideChatSetting = function () {
      $rootScope.$broadcast('hideSettingEvent');
      $scope.viewItem = 'teams';
    }

    $scope.hideDiscussionSetting = function () {
      $rootScope.$broadcast('hideDiscussionSettingEvent');
      $scope.viewItem = 'teams';
    }


    $scope.changed = function () {
      console.log("changed");
    }

    $scope.changeCreateDiscussion = function (value) {
      $scope.viewItem = value;
    }
    //$scope.tab1 = "active";
    $scope.imagetab1 = "images/signUp/chat-selected.png";
    $scope.imagetab2 = "images/signUp/other-chat-not selected.png";
    $scope.imagetab3 = "images/signUp/user-profile-notselected.png";

    $scope.producttab = function (tab) {
      $scope.producttabItem = tab;
    }

    // var chatMessagesRef = firebase.database().ref('node_chat_messages');
    var chatUserRef = firebase.database().ref('node_user').child($rootScope.loggedinuser.id);

    chatUserRef.on('child_changed', function (data) {
      $rootScope.updateUnreadMessagesInkbox();
      createTeamService.getmyteam($rootScope.AUTHTOKEN).then(function (response) {
        $rootScope.teamsForProfile = [];
        $rootScope.teamsForProfile = response;
      })

    });

    $scope.viewItem1 = false;
    $scope.setTeamsHighlight = function () {
      $scope.showteams = !$scope.showteams;
      //$scope.viewItem1 = $scope.viewItem1 ;
      //$scope.viewItem1 = !$scope.viewItem1 ;

      if ($scope.showteams == true) {
        $scope.viewItem1 = true;
      }

      if ($scope.showteams == false) {
        $scope.viewItem1 = false;
      }


      if ($scope.showteams == false && $state.current.url == "/productchats") {
        $scope.viewItem1 = true;
      }
    }


    $rootScope.teamsForProfile = [];
    $rootScope.progressBarText1 = "Loading Contacts";
    $rootScope.progressBarText2 = "Loading Chats";
    $rootScope.progressBarText3 = "Loading Messages";
    $rootScope.progressBarShow = true;
    createTeamService.getmyteam($rootScope.AUTHTOKEN).then(function (response) {
      $rootScope.progressBarShow = false;
      $rootScope.teamsForProfile = response;
    })


    $scope.setCurrentTeam = function (team) {

      console.log(team);
      $scope.viewItemTeam = team;
      $rootScope.currentTeam = team;
      $rootScope.updateUnreadMessagesForTeam();
      $rootScope.updateUnreadMessagesForTeamDiscussion();
      $scope.$broadcast('teamUpdated');
    }
    $scope.activeTab = function (item) {
      switch (item) {
        case 1: $scope.tab1 = "active";
          $scope.tab2 = "";
          $scope.tab3 = "";
          $scope.imagetab1 = "images/signUp/chat-selected.png";
          $scope.imagetab2 = "images/signUp/other-chat-not selected.png";
          $scope.imagetab3 = "images/signUp/user-profile-notselected.png";
          $scope.currentProfile = 'chats';
          break;
        case 2: $scope.tab2 = "active";
          $scope.tab1 = "";
          $scope.tab3 = "";
          $scope.imagetab1 = "images/signUp/chat-notselected.png";
          $scope.imagetab2 = "images/signUp/others-chat-selected.png";
          $scope.imagetab3 = "images/signUp/user-profile-notselected.png";
          $scope.currentProfile = 'discussions';
          break;
        case 3: $scope.tab3 = "active";
          $scope.tab2 = "";
          $scope.tab1 = "";
          $scope.imagetab1 = "images/signUp/chat-notselected.png";
          $scope.imagetab2 = "images/signUp/other-chat-not selected.png";
          $scope.imagetab3 = "images/signUp/user-profile-selected.png";
          $scope.currentProfile = undefined;
          break;
      }
    };

    switch ($state.current.name) {
      case 'profile.chats': $scope.activeTab(1);
        break;
      case 'profile.discussions': $scope.activeTab(2);
        break;
      case 'profile.myprofile': $scope.activeTab(3);
        break;
    }

    $scope.animationsEnabled = true;

    $scope.discussions = function () {
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'views/modals/discussionsModal.html',
        controller: 'ModalChatsCtrl',
      });

      modalInstance.result.then(function (selectedItem) {
        utilityService.serviceError(selectedItem);
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };



  });










/*angular.module('keitoApp').controller('productdiscussionsCtrl', function ($scope) {
  //var $ctrl = this;

});*/

angular.module('keitoApp').controller('productmyprofileCtrl', function ($rootScope, $scope, $uibModal, $log, utilityService, signInService, Users, createTeamService) {
  //var $ctrl = this;
  $scope.deleteChatVal = true;
  $scope.largeFormat = true;
  $scope.status = "Available";

  $scope.statusFormat = false;
  $scope.passwordFormat = false;

  if (Users.getTeamProfileSettings($rootScope.loggedinuser.id)[$rootScope.currentTeam]) {
    $scope.showProfilePictureTeam = Users.getTeamProfileSettings($rootScope.loggedinuser.id)[$rootScope.currentTeam].showProfilePicture;
  }
  else {
    $scope.showProfilePictureTeam = Users.getShowProfilePicture($rootScope.loggedinuser.id);
  }


  $scope.profilePictureViscibilityTeam = function () {

    if (!Users.getProfile($rootScope.loggedinuser.id)) {
      $scope.showProfilePictureTeam = false;
      utilityService.serviceError("Please Upload Profile Picture.");
      return;
    }
    var obj = {
      "showProfilePicture": $scope.showProfilePictureTeam
    }
    createTeamService.updateUserProfileToTeam($rootScope.AUTHTOKEN, $rootScope.currentTeam, obj).then(function (response) {
      $rootScope.progressBarShow = false;
      utilityService.serviceError("Profile setting updated successfully.");
    })

  }

  $scope.setStatus = function (status) {
    console.log(status);
    if (!$scope.statusFormat) {
      $scope.largeFormat = false;
      $scope.statusFormat = true;
      $scope.passwordFormat = false;
    }
    else {
      $scope.largeFormat = true;
      $scope.statusFormat = false;
      $scope.passwordFormat = false;
    }
  }

  $scope.getStatusClass = function (status) {
    switch (status) {
      case "Available": return "statusAvailable";
      case "Busy": return "statusBusy";
      case "Away": return "statusAway";
      case "Do no disturb": return "statusDoNotDisturb";
      case "Not At Work": return "statusNotAtWork"
    }
  }

  $scope.setTeamStatusStatusDone = function (status) {
    $scope.largeFormat = true;
    $scope.statusFormat = false;
    $scope.passwordFormat = false;
    $scope.status = status;
    console.log(status);

    var obj = {
      "status": status
    }
    createTeamService.updateUserProfileToTeam($rootScope.AUTHTOKEN, $rootScope.currentTeam, obj).then(function (response) {
      $rootScope.progressBarShow = false;
      utilityService.serviceError("Status updated successfully.");
    })

  }

  $scope.resendVerificationMail = function (email) {
    console.log(email);
    var obj = {
      "email": email
    }

    $rootScope.progressBarText1 = "Fecthing Mail Id";
    $rootScope.progressBarText2 = "Validating Mail Id";
    $rootScope.progressBarText3 = "Sending Mail Confirmation";
    $rootScope.progressBarShow = true;

    signInService.resendMailConfirmation(obj).then(function (response) {
      $rootScope.progressBarShow = false;
      utilityService.serviceError("Verification mail sent successfully.");


    })


  }

  $scope.deleteChat = function () {
    console.log($scope.deleteChatVal);

    if (!$scope.deleteChatVal) {
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'views/modals/deleteChatsModal.html',
        controller: 'deleteChatsModalCtrl'/*,
              /*controllerAs: '$ctrl',
              
              resolve: {
                items: function () {
                  return $ctrl.items;
                }
              }*/
      });

      modalInstance.result.then(function (selectedItem) {
        utilityService.serviceError(selectedItem);
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    }
  }

  $scope.Users = Users;
  $scope.uploadFile = function (files) {
    var file = files[0];
    var reader = new FileReader();

    var ImageRef = firebase.database().ref('ProfileImages').child($rootScope.loggedinuser.id);

    var storageRef = firebase.storage().ref();

    // Create a reference to 'mountains.jpg'
    var mountainsRef = storageRef.child('Profile_Images');

    // Create a reference to 'images/mountains.jpg'
    var mountainImagesRef = storageRef.child('Profile_Images/mountains.jpg');


    reader.addEventListener("load", function () {
      //preview.src = reader.result;
      ImageRef.set({
        'image': reader.result
      }, function (snap) {
        console.log(snap);
        utilityService.serviceError("profile picture updated succesfully")
      })

      mountainImagesRef.putString(reader.result, 'data_url').then(function (snapshot) {
        console.log('Uploaded a base64url string!');
      });

    });



    reader.readAsDataURL(file);

  }


});

angular.module('keitoApp').directive('fileModel', ['$parse', function ($parse) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var model = $parse(attrs.fileModel);
      var modelSetter = model.assign;

      element.bind('change', function () {
        scope.$apply(function () {
          modelSetter(scope, element[0].files[0]);
        });
      });
    }
  };
}]);