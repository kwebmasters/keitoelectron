angular.module('keitoApp').controller('createDiscussionCtrl', function ($scope,$state,Users,discussionService,$rootScope,$uibModal,$log,utilityService,createTeamService) {

     $rootScope.progressBarText1 = "Loading Teams";
    $rootScope.progressBarText2 = "Loading Teams Details";
    $rootScope.progressBarText3 = "Loading Teams Members";
    $rootScope.progressBarShow = true;

    createTeamService.getTeamById($rootScope.AUTHTOKEN,$rootScope.currentTeam).then(function(response) {
        
        $rootScope.teamForCreateDiscussion = response;

        $rootScope.progressBarShow = false;

        for(var i=0;i<$scope.teamForCreateDiscussion.members.length;i++) {
		        $scope.checkboxValues[$scope.teamForCreateDiscussion.members[i].userId] = true;
		    }
		$scope.teamChecked = true ;
      	$scope.fullTeamChecked = true;
    })
    $scope.checkboxValues = [];
    $scope.discussionForm = {
    	"subject" : undefined,
        "description" :undefined
    }

     $scope.checkboxChanged = function(userId) {      

      var flag = true;
      if($scope.checkboxValues == undefined) {
      	
      }

      if($scope.checkboxValues[userId]) {
      	$scope.checkboxValues[userId] = false
      }
      else {
      	$scope.checkboxValues[userId] = true
      }
      
      for(var i=0;i<$scope.teamForCreateDiscussion.members.length;i++) {
        if(!$scope.checkboxValues[$scope.teamForCreateDiscussion.members[i].userId] && $scope.teamForCreateDiscussion.members[i].userId!= $rootScope.loggedinuser.id) {
            flag = false;

            break;
        }
      }
      $scope.teamChecked = flag ;
      $scope.fullTeamChecked = flag;

    }

    $scope.masterCheckboxClicked =  function(value) {
    	$scope.teamChecked = value ;



    	if(value) {
    		for(var i=0;i<$scope.teamForCreateDiscussion.members.length;i++) {
		        $scope.checkboxValues[$scope.teamForCreateDiscussion.members[i].userId] = true;
		    }
    	}
    	else {
    		for(var i=0;i<$scope.teamForCreateDiscussion.members.length;i++) {
		        $scope.checkboxValues[$scope.teamForCreateDiscussion.members[i].userId] = false;
		    }
    	}
    	
    }

    $scope.addDiscussion = function(subject,description){
    	
    	if($scope.discussionForm.subject == undefined) {
    		utilityService.serviceError("Please enter subject");
      		return;
    	}

    	if($scope.discussionForm.description == undefined) {
    		utilityService.serviceError("Please enter description");
      		return;

    	}
    	if($scope.teamChecked) {
			var obj ={
		        "type" : "DISCUSSION",
		        "teamId" : $rootScope.currentTeam
			}
			$rootScope.progressBarText1 = "Creating Discussion";
		    $rootScope.progressBarText2 = "Adding Team";
		    $rootScope.progressBarText3 = "Adding Members";
		    $rootScope.progressBarShow = true;
    		discussionService.intiateDiscussion(obj,$rootScope.AUTHTOKEN).then(function(response) {
		           //$rootScope.progressBarShow = false;
		           firebase.database().ref('node_chat').child(response.id).update(
		            {
		                type:'discussion',
		                subject:$scope.discussionForm.subject,
		                description:$scope.discussionForm.description,
		                createdBy:$rootScope.loggedinuser.name,
		                createdOn:firebase.database.ServerValue.TIMESTAMP,
		                lastActivityTimestamp:firebase.database.ServerValue.TIMESTAMP,
              			lastActivityBy:$rootScope.loggedinuser.name

		            },function (err) {
					    //console.log("callback complete! ", err);
					    $rootScope.progressBarShow = false;
					    $scope.discussionForm.subject = undefined;
						$scope.discussionForm.description = undefined;
						$scope.changeCreateDiscussion('teams');
						$state.go('profile.productdiscussions');
						$rootScope.fromDiscussion = response.id;
					});

		          
					//$scope.discussionForm.subject = undefined;
					//$scope.discussionForm.description = undefined;
					//$scope.changeCreateDiscussion('teams');
					//$state.go('profile.productdiscussions');
		    }) 
		}
		else {

			var disMembers = [];
			for(var i=0;i<$scope.teamForCreateDiscussion.members.length;i++) {
		        if($scope.checkboxValues[$scope.teamForCreateDiscussion.members[i].userId]) {
		            disMembers.push($scope.teamForCreateDiscussion.members[i].userId);
		        }
		    }

		    if(disMembers.length == 0) {
		    	utilityService.serviceError("Please select members");
      			return;
		    }

			disMembers.push($rootScope.loggedinuser.id);

		    var obj ={
					    "type" : "GROUP",
					    "teamId" : $rootScope.currentTeam,
					    "subject" : $scope.discussionForm.subject,
					    "memberIds" : disMembers

					}
			$rootScope.progressBarText1 = "Creating Discussion";
		    $rootScope.progressBarText2 = "Adding Team";
		    $rootScope.progressBarText3 = "Adding Members";
		    $rootScope.progressBarShow = true;
    		discussionService.intiateDiscussion(obj,$rootScope.AUTHTOKEN).then(function(response) {
		           
		           firebase.database().ref('node_chat').child(response.id).set(
		            {
		            	type:'group_chat',
		            	subject:$scope.discussionForm.subject,
		                description:$scope.discussionForm.description,
		                createdBy:$rootScope.loggedinuser.name,
		                createdOn:firebase.database.ServerValue.TIMESTAMP,
		                lastActivityTimestamp:firebase.database.ServerValue.TIMESTAMP,
              			lastActivityBy:$rootScope.loggedinuser.name
		            },function (err) {
					    //console.log("callback complete! ", err);
					    $rootScope.progressBarShow = false;
					    $scope.discussionForm.subject = undefined;
						$scope.discussionForm.description = undefined;
						$scope.changeCreateDiscussion('teams');
						$state.go('profile.productdiscussions');
						$rootScope.fromDiscussion = response.id;
					})
		           
		    }) 


		}

    }


});