angular.module('keitoApp').factory('discussionService', ['$firebaseArray', '$firebaseObject','$q','utilityService','$http', 'Users',
  function($firebaseArray,$firebaseObject,$q,utilityService,$http ,Users) {
    var usersRef = firebase.database().ref('node_user');

    var userDiscussionsRef = firebase.database().ref('node_chat');
    var discussion_url = "http://test-apis.keito.in/chat";
    var chatMessagesRef = firebase.database().ref('node_chat_messages');
    
    var userDiscussionDetailsRef = $firebaseArray(userDiscussionsRef);
   
    var discussionServices =  {
      userDiscussionDetailsRef :$firebaseArray(userDiscussionsRef),
      intiateDiscussion: function(data,token){
          var deferred = $q.defer();
           $http({
                      method: "POST",
                      headers: {'Content-Type': 'application/json;','AUTH-TOKEN':token},
                      url:discussion_url,
                      data: data
                  })
             .success(function(data) { 
                deferred.resolve(data);

             }).error(function(msg, code) {
                deferred.reject(msg);
                utilityService.serviceError(msg.errors[0]);
             });
           return deferred.promise;
      },
      getAllDiscussions: function(uid1){
        var path =  uid1+'/DISCUSSIONS';
        return $firebaseArray(usersRef.child(path));
      },
      getMembersOfDiscussion: function(url) {
        return $firebaseArray(firebase.database().ref('node_chat').child(url).child('membersList'));
      },
      getAllGroupChats: function(uid1){
        var path =  uid1+'/GROUP_CHATS';
        return $firebaseArray(usersRef.child(path));
      },
      pushFile: function(obj,sender,url){
        firebase.database().ref('node_chat_messages').child(url).push(obj);
        firebase.database().ref('node_chat').child(url).update(
          {
              lastActivityTimestamp:firebase.database.ServerValue.TIMESTAMP,
              lastActivityBy:sender
          });
      },
      pushDiscussion: function(message,sender,url){
        firebase.database().ref('node_chat_messages').child(url).push(
          {
              time_stamp:firebase.database.ServerValue.TIMESTAMP,
              message_text:message,
              sender_user_id :sender,
              message_type : "text",
              device : "chrome"
          });
        firebase.database().ref('node_chat').child(url).update(
          {
              lastActivityTimestamp:firebase.database.ServerValue.TIMESTAMP,
              lastActivityBy:sender
          });
      },
      getCreatedBy: function(url){
        
        if(userDiscussionDetailsRef.$getRecord(url) == undefined || userDiscussionDetailsRef.$getRecord(url)== null) {
          return
        }

        return userDiscussionDetailsRef.$getRecord(url).createdBy;
      },

      getLastMessageNode: function(url){
        return $firebaseArray(firebase.database().ref('node_chat_messages').child(url).limitToLast(1));
      },

      updateLastReadNode :function(uid1,discussion_id,type,chat_id){
        if(type == "GROUP") {
          type ="GROUP_CHATS";
        }

        firebase.database().ref('node_user').child(uid1).child(type).child(discussion_id).update(
          {
              last_read_message_id:chat_id,
          });
      },

      getLastActivity: function(url){
        if(userDiscussionDetailsRef.$getRecord(url) == undefined || userDiscussionDetailsRef.$getRecord(url)== null) {
          return
        }
        return userDiscussionDetailsRef.$getRecord(url).lastActivityTimestamp;

      },

      getSubject: function(url){
        if(url == undefined || url== null) {
          return
        }

        return userDiscussionDetailsRef.$getRecord(url).subject;
      },
      getCreatedOn: function(url){
        if(userDiscussionDetailsRef.$getRecord(url) == undefined || userDiscussionDetailsRef.$getRecord(url)== null) {
          return
        }
        return userDiscussionDetailsRef.$getRecord(url).createdOn;
      },
      getLastTimestamp: function(url){
        if(url == undefined || url== null) {
          return
        }
        
        return userDiscussionDetailsRef.$getRecord(url).lastTimestamp;
      },
      getDisplayName: function(url){
        if(url == undefined || url== null) {
          return
        }
        return Users.getDisplayName(userDiscussionDetailsRef.$getRecord(url).createdBy);
      },
      pushMessage: function(message,sender,url){
        var deferred = $q.defer();
        var obj = {
              time_stamp:firebase.database.ServerValue.TIMESTAMP,
              message_text:message,
              sender_user_id :sender,
              message_type : "text",
              device : "chrome"
          }
        firebase.database().ref('node_chat_messages').child(url).push(obj,function (err) {
              deferred.resolve();
          });

        firebase.database().ref('node_chat').child(url).update(
          {
              lastActivityTimestamp:firebase.database.ServerValue.TIMESTAMP,
              lastActivityBy:sender
          });
        firebase.database().ref('node_chat').child(url).child('last_message').set(obj);

        return deferred.promise;

      },
      getDiscussionMessages: function(url){
        var path =  url+'/messages' ;

        return $firebaseArray(chatMessagesRef.child(url));
      }

    };

    return discussionServices;

    }
  ]);