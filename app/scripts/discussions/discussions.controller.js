angular.module('keitoApp').controller('productdiscussionsCtrl', function (focus,$state,$scope,Users,discussionService,$rootScope,$uibModal,$log,utilityService,createTeamService,chatService) {
	$scope.discussionsModal = function () {
    $scope.changeCreateDiscussion('createDiscussion');
    $state.go('profile.createDiscussion');
  };

  $scope.producttab('Discussions');

  $scope.handlePaste = function(event,name) {

    chatService.uploadFile(event,$rootScope.AUTHTOKEN).then(function(response) {
       $rootScope.progressBarText1 = "Loading File";
      $rootScope.progressBarText2 = "Fetching Details";
      $rootScope.progressBarText3 = "Uploading File";
      $rootScope.progressBarShow = true;
      var obj = {
              time_stamp:firebase.database.ServerValue.TIMESTAMP,
              message_text:name,
              sender_user_id : $scope.loggedinuser.id,
              message_type : "file",
              fileID : response.path,
              device : "chrome"
          }

        chatService.pushFile(typedmessage,$scope.loggedinuser.id,$scope.prevUser.chat_id).then(function() {
               
               $rootScope.progressBarShow = false;
               firebase.database().ref('node_user').child($scope.prevUser.$id).child('INDIVIDUAL_CHATS').child($scope.loggedinuser.id).update({
                    unread_msg_count:userDetailsRef.$getRecord($scope.prevUser.$id).INDIVIDUAL_CHATS[$scope.loggedinuser.id].unread_msg_count+1
                });
                firebase.database().ref('node_user').child($scope.loggedinuser.id).child('INDIVIDUAL_CHATS').child($scope.prevUser.$id).update({
                    unread_msg_count:0
                });      
            }) 
      })
  }

  $scope.showDiscussionSetting = function() {
    $scope.showSettingsDiv = true;
    $scope.setViewSettingSViewItem('Discussion Settings'); 
  }
 $rootScope.$on('hideDiscussionSettingEvent', function(e) {  
        $scope.showSettingsDiv = false;            
    });


  $scope.userService =  Users;
    $scope.utilityService = utilityService;
    $scope.discussionService = discussionService;
    $scope.chatService = chatService;

    $scope.getDiscussionsByTeamId = function(chatUsers,teamChats) {
      var finalChats = [];
      for (var i = 0; i < teamChats.length; i++) {
        for (var j = 0; j < chatUsers.length; j++) {
          if(chatUsers[j].$id == teamChats[i].id){
            finalChats.push(chatUsers[j]);
            break;
          }    
        }
      }

      return finalChats;
    }

    var chatMessagesRef = firebase.database().ref('node_chat_messages');
    var chatUserRef = firebase.database().ref('node_user').child($rootScope.loggedinuser.id);

    chatUserRef.on('child_changed', function(data) {
     //$rootScope.updateUnreadMessagesInkbox();
     $rootScope.updateUnreadMessagesForTeamDiscussion();
      $scope.loadDiscussions();
      /* createTeamService.getmyteam($rootScope.AUTHTOKEN).then(function(response) {
              $rootScope.teamsForProfile  = [];
              $rootScope.teamsForProfile =response.data;         
          })*/

    });

  $scope.loadDiscussions = function() {
      $rootScope.progressBarText1 = "Loading Discussions";
      $rootScope.progressBarText2 = "Loading Chats";
      $rootScope.progressBarText3 = "Loading Messages";
      $rootScope.progressBarShow = true;
    chatService.getChatsByTeamId($rootScope.currentTeam,$rootScope.AUTHTOKEN).then(function(response) {
        var teamDiscussions = [];
        //teamDiscussions = response.DISCUSSION.concat(response.GROUP);
        discussionService.getAllDiscussions($scope.loggedinuser.id).$loaded().then(function(teamDiscussionUsers) {
          
          $scope.discussionUsers = teamDiscussionUsers;
          discussionService.getAllGroupChats($scope.loggedinuser.id).$loaded().then(function(teamGroupChatUsers) {
              $scope.teamGroupChatUsers = teamGroupChatUsers;
              

               $scope.discussionUsers.push.apply( teamDiscussionUsers , teamGroupChatUsers)

               

              discussionService.userDiscussionDetailsRef.$loaded().then(function(Message) {
                 

                 $rootScope.progressBarShow = false;
                 $scope.loadDiscussionUsers = true;

                 for(var k=0;k<$scope.discussionUsers.length;k++) {
                   if($scope.discussionUsers[k].team_id == $rootScope.currentTeam) {
                      $scope.showChat($scope.discussionUsers[k],k);
                      $scope.showSmallFormat(true);
                      break;
                   }
                 }

                 

                 if($rootScope.fromDiscussion != undefined) {
                   for(var i=0; i<$scope.discussionUsers.length;i++) {
                    if($scope.discussionUsers[i].$id == $rootScope.fromDiscussion) {
                      $scope.showChat($scope.discussionUsers[i],i);
                      $scope.showSmallFormat(true);
                      $rootScope.fromDiscussion = undefined;
                    }
                   }
                 }

              });

          });
      });

    });
    };
    $scope.currentSelectedDiscussionFlag = 0;
    $scope.onKeydownChild = function(evt) {
      switch(evt.keyCode) {
        case 13:if($scope.currentSelectedDiscussionFlag != 0 && !$scope.smallFormat) { 
                  $scope.showChat($scope.discussionUsers[$scope.currentSelectedDiscussionFlag-1],$scope.currentSelectedDiscussionFlag-1);
                  $scope.showSmallFormat(true,$scope.currentSelectedDiscussionFlag-1);
                  break;
                }
                break;

        case 38: if($scope.currentSelectedDiscussionFlag != 0 && $scope.currentSelectedDiscussionFlag!=1) {
                      $scope.currentSelectedDiscussionFlag--;
                   }
                 break;

        case 40: if($scope.currentSelectedDiscussionFlag != $scope.discussionUsers.length) {
                      $scope.currentSelectedDiscussionFlag++;
                   }
                 break;
      }

      if (evt.altKey && evt.keyCode == 68) {
            //console.log('alt+c');
            $scope.discussionsModal();
        }
    };

    $scope.setRowcolor = function(index) {
      if(index == $scope.currentSelectedDiscussionFlag-1) {
        return { 'background-color':'#c0c0c0' }; 
      }
    }


    /*$scope.$watch('$scope.teamDiscussionUsers', function (newVal, oldVal) {
      $scope.loadDiscussions();

    }, true);
    $scope.$watch('$scope.teamGroupChatUsers', function (newVal, oldVal) {
      $scope.loadDiscussions();

    }, true); */


  $scope.loadDiscussions();

  $scope.checkWhetherToday = function(epochDate) {
     var todaysDate = new Date();

     var epoch = epochDate/1000;
          var inputDate = new Date(0); // The 0 there is the key, which sets the date to the epoch
          inputDate.setUTCSeconds(epoch);

     if(inputDate.setHours(0,0,0,0) == todaysDate.setHours(0,0,0,0)) {
         return "showThisDateFormat";
      }
      else {
        return "DntshowThisDateFormat";
      }
  }

  $scope.checkWhetherNtToday = function(epochDate) {
     var todaysDate = new Date();

     var epoch = epochDate/1000;
          var inputDate = new Date(0); // The 0 there is the key, which sets the date to the epoch
          inputDate.setUTCSeconds(epoch);

    if(inputDate.setHours(0,0,0,0) != todaysDate.setHours(0,0,0,0)) {
         return "showThisDateFormat";
    }
    else {
      return "DntshowThisDateFormat";
    }

  }

  $scope.getClass = function(x) {
    if(x== $rootScope.loggedinuser.id) {
      return "message-personal";
    }
  }

  $scope.set_color = function (user) {
  if($scope.prevUser != undefined) {
    if ($scope.prevUser.$id == user.$id) {
      return { 'background-color':'#c0c0c0' }
    }
  };
  }

  $scope.showChat = function (user,index) {

    focus('focusDiscussionInput');

    $scope.sendingmessage = undefined;
    $scope.prevUser = user;
    var chat_type = "DISCUSSIONS";
    $scope.currentSelectedDiscussionFlag = index+1;
    if( $scope.prevUser.chat_type == 'GROUP') {
          chat_type ="GROUP_CHATS"
    }
    firebase.database().ref('node_user').child($scope.loggedinuser.id).child(chat_type).child($scope.prevUser.$id).update({
        unread_msg_count:0
    });
    discussionService.getDiscussionMessages($scope.prevUser.$id).$loaded().then(function(discussionMessages) {
        $scope.discussionMessages = discussionMessages;     
    });
  }

  $scope.showSmallFormat = function(format,user) {
    $scope.sendingmessage = undefined;

    if(format) {
      $scope.smallFormat = true;
    }
    else {
      if($scope.prevUser!= undefined) {
        if($scope.prevUser.$id == user.$id) {
          $scope.prevUser = undefined;
          $scope.smallFormat = false;
        }
      }
    }
  }

  $scope.lastReadMessageNode = function() {
    discussionService.getLastMessageNode($scope.prevUser.$id).$loaded().then(function(chatMessages) {
          if(chatMessages.length!= 0){
              discussionService.updateLastReadNode($scope.loggedinuser.id,$scope.prevUser.$id,$scope.prevUser.chat_type,chatMessages[0].$id);//(uid1,discussion_id,type,chat_id)

            }      
        });
  }

  $scope.sendMessage = function(sendingmessage) {
    if(sendingmessage == undefined || sendingmessage == null || sendingmessage == '' ){
            utilityService.serviceError("Please Enter Message");
            return;
          }
    var chat_type = "DISCUSSIONS";
    discussionService.pushMessage(sendingmessage,$scope.loggedinuser.id,$scope.prevUser.$id).then(function() {
      //$scope.lastReadMessageNode();
      discussionService.getMembersOfDiscussion($scope.prevUser.$id).$loaded().then(function(members) {
          if( $scope.prevUser.chat_type == 'GROUP') {
            chat_type ="GROUP_CHATS"
          }

          if($scope.prevUser.chat_type == 'GROUP') {
            for (var i = 0; i < members.length; i++) {
              if(members[i].$id != $scope.loggedinuser.id){
                firebase.database().ref('node_user').child(members[i].$id).child('GROUP_CHATS').child($scope.prevUser.$id).update({
                    unread_msg_count:userDetailsRef.$getRecord(members[i].$id).GROUP_CHATS[$scope.prevUser.$id].unread_msg_count+1
                });
                 
              }
            }
          }
          else {
            for (var i = 0; i < members.length; i++) {
              if(members[i].$id != $scope.loggedinuser.id){
                firebase.database().ref('node_user').child(members[i].$id).child('DISCUSSIONS').child($scope.prevUser.$id).update({
                    unread_msg_count:userDetailsRef.$getRecord(members[i].$id).DISCUSSIONS[$scope.prevUser.$id].unread_msg_count+1
                });
                 
              }
            }
          }
        

          firebase.database().ref('node_user').child($scope.loggedinuser.id).child(chat_type).child($scope.prevUser.$id).update({
                  unread_msg_count:0
              });

         /* if(chatMessages.length!= 0){
              discussionService.updateLastReadNode($scope.loggedinuser.id,$scope.prevUser.$id,$scope.prevUser.chat_type,chatMessages[0].$id);//(uid1,discussion_id,type,chat_id)

            }      */
        });


      $scope.sendingmessage = undefined;
    })
    
  }

});



angular.module('keitoApp').controller('modalDiscussionCtrl', function ($uibModalInstance,$scope,utilityService) {
  $scope.ok = function () {

    if($scope.subject == undefined || $scope.subject == null){
            utilityService.serviceError("Please Enter Subject");
            return;
          }
    if($scope.description == undefined || $scope.description == null){
            utilityService.serviceError("Please Enter Description");
            return;
          }

    var obj= {
      "subject" :$scope.subject,
      "description" : $scope.description 
    }
    $uibModalInstance.close(obj);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});


