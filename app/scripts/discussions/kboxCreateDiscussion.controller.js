angular.module('keitoApp').controller('kboxCreateDiscussionCtrl', function ($scope,$state,Users,discussionService,$rootScope,$uibModal,$log,utilityService,createTeamService) {

     $rootScope.progressBarText1 = "Loading Teams";
    $rootScope.progressBarText2 = "Loading Teams Details";
    $rootScope.progressBarText3 = "Loading Teams Members";
    $rootScope.progressBarShow = true;

    createTeamService.getmyteam($rootScope.AUTHTOKEN).then(function(response) {
        
        $rootScope.teamForCreateDiscussion = response;

        $rootScope.progressBarShow = false;
    })
    $scope.checkboxValues = [];
    $scope.discussionForm = {
    	"subject" : undefined,
        "description" :undefined
    }

    $scope.checkboxTeams = {};
    $scope.checkboxTeamMembers = {};



     $scope.checkboxChanged = function(userId,team) { 

      	if($scope.prevTeamId != team.id ) {
    		$scope.prevTeamId = team.id;
    		$scope.checkboxTeamMembers = {};
    		$scope.checkboxTeams = {};
    		$scope.checkboxTeamMembers[userId+'_'+team.id] = true;
    	}
    	if(team.members.length-1 == Object.keys($scope.checkboxTeamMembers).length) {
     	$scope.setMaster = true;

     	angular.forEach($scope.checkboxTeamMembers, function(value, key) {
		  if($scope.checkboxTeamMembers[key] == false) {
		  	$scope.setMaster = false;
		  }
		});

		if($scope.setMaster) {
			$scope.checkboxTeams[team.id] = true;
		}
		else {
			$scope.checkboxTeams= {};
		}
     }
     else {
     	$scope.checkboxTeams= {};
     }

     
    	 

      /*var flag = true;
      if($scope.checkboxValues == undefined) {
      	
      }

      if($scope.checkboxValues[userId]) {
      	$scope.checkboxValues[userId] = false
      }
      else {
      	$scope.checkboxValues[userId] = true
      }
      //$scope.checkboxValues[userId] = !$scope.checkboxValues[userId];

      for(var i=0;i<$scope.teamForCreateDiscussion.members.length;i++) {
        if(!$scope.checkboxValues[$scope.teamForCreateDiscussion.members[i].userId] && $scope.teamForCreateDiscussion.members[i].userId!= $rootScope.loggedinuser.id) {
            flag = false;

            break;
        }
      }
      $scope.teamChecked = flag ;
      $scope.fullTeamChecked = flag; */

    }

   	$scope.prevTeamId;
    $scope.masterCheckboxClicked =  function(teamid,setCheckBox) {
    	if(setCheckBox) {
    		 if($scope.prevTeamId != teamid ) {
	    		$scope.checkboxTeamMembers = {};
	    		$scope.checkboxTeams = {};
	    	}
    		 $scope.checkboxTeams = {};
    		 $scope.checkboxTeams[teamid] = setCheckBox;
    		 //$scope.checkboxTeams = {};
    		 //$scope.checkboxTeamMembers = {};

    	}
    	else
    	{
    		$scope.checkboxTeams = {};
    		$scope.checkboxTeamMembers = {};
    		$scope.prevTeamId = undefined;
    	}

    	
    	 
    	/*$scope.teamChecked = value ;



    	if(value) {
    		for(var i=0;i<$scope.teamForCreateDiscussion.members.length;i++) {
		        $scope.checkboxValues[$scope.teamForCreateDiscussion.members[i].userId] = true;
		    }
    	}
    	else {
    		for(var i=0;i<$scope.teamForCreateDiscussion.members.length;i++) {
		        $scope.checkboxValues[$scope.teamForCreateDiscussion.members[i].userId] = false;
		    }
    	}*/
    	
    }

    $scope.addDiscussion = function(subject,description){
    	
    	if($scope.discussionForm.subject == undefined) {
    		utilityService.serviceError("Please enter subject");
      		return;
    	}

    	if($scope.discussionForm.description == undefined) {
    		utilityService.serviceError("Please enter description");
      		return;

    	}
    	if(Object.keys($scope.checkboxTeams).length!=0) {
			var obj ={
		        "type" : "DISCUSSION",
		        "teamId" : Object.keys($scope.checkboxTeams)[0]
			}
			$rootScope.progressBarText1 = "Creating Discussion";
		    $rootScope.progressBarText2 = "Adding Team";
		    $rootScope.progressBarText3 = "Adding Members";
		    $rootScope.progressBarShow = true;
    		discussionService.intiateDiscussion(obj,$rootScope.AUTHTOKEN).then(function(response) {
		           //$rootScope.progressBarShow = false;
		           firebase.database().ref('node_chat').child(response.id).update(
		            {
		                type:'discussion',
		                subject:$scope.discussionForm.subject,
		                description:$scope.discussionForm.description,
		                createdBy:$rootScope.loggedinuser.name,
		                createdOn:firebase.database.ServerValue.TIMESTAMP,
		                lastActivityTimestamp:firebase.database.ServerValue.TIMESTAMP,
              			lastActivityBy:$rootScope.loggedinuser.name

		            },function (err) {
					    //console.log("callback complete! ", err);
					    $rootScope.progressBarShow = false;
					    $scope.discussionForm.subject = undefined;
						$scope.discussionForm.description = undefined;
						$scope.changeCreateDiscussion('teams');
            $scope.setViewItem('teams');
            $scope.setCurrentTeam(Object.keys($scope.checkboxTeams)[0]);
            $state.go('profile.productdiscussions');

            $scope.setTeamsHighlight();


						$rootScope.fromDiscussion = response.id;
					});

		          
					//$scope.discussionForm.subject = undefined;
					//$scope.discussionForm.description = undefined;
					//$scope.changeCreateDiscussion('teams');
					//$state.go('profile.productdiscussions');
		    }) 
		}
		else {

			var disMembers = [];
			

        angular.forEach($scope.checkboxTeamMembers, function(value, key) {
              if($scope.checkboxTeamMembers[key] != false) {
                var id = key.split("_");
                disMembers.push(id[0]);
              }
            });


		    if(disMembers.length == 0) {
		    	utilityService.serviceError("Please select members");
      			return;
		    }

			disMembers.push($rootScope.loggedinuser.id);

		    var obj ={
					    "type" : "GROUP",
					    "teamId" : $scope.prevTeamId,
					    "subject" : $scope.discussionForm.subject,
					    "memberIds" : disMembers

					}
			$rootScope.progressBarText1 = "Creating Discussion";
		    $rootScope.progressBarText2 = "Adding Team";
		    $rootScope.progressBarText3 = "Adding Members";
		    $rootScope.progressBarShow = true;
    		discussionService.intiateDiscussion(obj,$rootScope.AUTHTOKEN).then(function(response) {
		           
		           firebase.database().ref('node_chat').child(response.id).set(
		            {
		            	type:'group_chat',
		            	subject:$scope.discussionForm.subject,
		                description:$scope.discussionForm.description,
		                createdBy:$rootScope.loggedinuser.name,
		                createdOn:firebase.database.ServerValue.TIMESTAMP,
		                lastActivityTimestamp:firebase.database.ServerValue.TIMESTAMP,
              			lastActivityBy:$rootScope.loggedinuser.name
		            },function (err) {
					    //console.log("callback complete! ", err);
					    $rootScope.progressBarShow = false;
					    $scope.discussionForm.subject = undefined;
						$scope.discussionForm.description = undefined;
						$scope.changeCreateDiscussion('teams');
						$scope.setCurrentTeam($scope.prevTeamId);
            $scope.setTeamsHighlight();
            $state.go('profile.productdiscussions');
            $scope.setViewItem('teams');
						$rootScope.fromDiscussion = response.id;
					})
		           
		    }) 


		}

    }


});