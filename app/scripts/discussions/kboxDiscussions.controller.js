angular.module('keitoApp').controller('kboxDiscussionsCtrl', function ($state,$scope,Users,discussionService,$rootScope,$uibModal,$log,utilityService,createTeamService,chatService) {
	$scope.discussionsModal = function () {
    	$scope.changeCreateDiscussion('kboxCreateDiscussion');
    	$state.go('profile.kboxCreateDiscussoins');
  	}

  	$scope.discussionService = discussionService;

  	$scope.utilityService = utilityService;
	$scope.userService = Users;
	$scope.chatService = chatService;
	$scope.getCountOfUnreadMsgs = function (id) {
		return chatService.getCountOfUnreadMsgs($scope.loggedinuser.id,id)
	}

   

	var chatMessagesRef = firebase.database().ref('node_chat_messages');
    var chatUserRef = firebase.database().ref('node_user').child($rootScope.loggedinuser.id);

    chatUserRef.on('child_changed', function(data) {
     //$rootScope.updateUnreadMessagesInkbox();
     $rootScope.updateUnreadMessagesForTeam();
    });

  $scope.loadDiscussions = function() {
		

	  $rootScope.progressBarText1 = "Loading Discussions";
      $rootScope.progressBarText2 = "Loading Chats";
      $rootScope.progressBarText3 = "Loading Messages";
      $rootScope.progressBarShow = true;

    discussionService.getAllGroupChats($scope.loggedinuser.id).$loaded().then(function(groupChats) {
       discussionService.getAllDiscussions($scope.loggedinuser.id).$loaded().then(function(discussions) {
       		chatService.userDetailsRef.$loaded().then(function() { 
       			$rootScope.progressBarShow = false;
       			$scope.allDiscussionUsers =[];
		        $scope.allDiscussionUsers = groupChats;
		        $scope.allDiscussionUsers.push.apply(groupChats,discussions);
		        $scope.discussionsLoaded = true;
		    });
	    });

    });		
}

 $scope.checkWhetherToday = function(epochDate) {
     var todaysDate = new Date();

     var epoch = epochDate/1000;
          var inputDate = new Date(0); // The 0 there is the key, which sets the date to the epoch
          inputDate.setUTCSeconds(epoch);

     if(inputDate.setHours(0,0,0,0) == todaysDate.setHours(0,0,0,0)) {
         return "showThisDateFormat";
      }
      else {
        return "DntshowThisDateFormat";
      }
  }

  $scope.checkWhetherNtToday = function(epochDate) {
     var todaysDate = new Date();

     var epoch = epochDate/1000;
          var inputDate = new Date(0); // The 0 there is the key, which sets the date to the epoch
          inputDate.setUTCSeconds(epoch);

    if(inputDate.setHours(0,0,0,0) != todaysDate.setHours(0,0,0,0)) {
         return "showThisDateFormat";
    }
    else {
      return "DntshowThisDateFormat";
    }

  }

    $scope.loadDiscussions();

    $scope.showDiscussion =function(discussions) {
    	       $state.go('profile.productdiscussions');
                $scope.producttab('Discussions');
                $scope.setTeamsHighlight();
                $scope.setViewItem('teams');
                $scope.setCurrentTeam(discussions.team_id);
                $rootScope.fromDiscussion = discussions.$id;
      
    }

});