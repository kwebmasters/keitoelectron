angular.module('keitoApp').controller('kboxCreateChatsCtrl', function ($scope,$state,Users,chatService,discussionService,$rootScope,$uibModal,$log,utilityService,createTeamService) {

     $rootScope.progressBarText1 = "Loading Teams";
    $rootScope.progressBarText2 = "Loading Teams Details";
    $rootScope.progressBarText3 = "Loading Teams Members";
    $rootScope.progressBarShow = true;

    createTeamService.getmyteam($rootScope.AUTHTOKEN).then(function(response) {
        
        $rootScope.teamsForCreateChats = response;

        $rootScope.progressBarShow = false;
    })
    $scope.radioBtnChanged = function(user,teamId) {
    	$scope.selectedUser = user;
    	$scope.selectedTeam = teamId;
    	$scope.intiateChat();
    };

    $scope.intiateChat = function() {
    	console.log($scope.selectedUser);

    	$rootScope.progressBarText1 = "Loading Chats";
    	$rootScope.progressBarText2 = "Intiating New Chat";
    	$rootScope.progressBarText3 = "Getting Chat Details";
    	$rootScope.progressBarShow = true;


    	chatService.getChatHeadById($scope.loggedinuser.id,$scope.selectedUser).$loaded().then(function(chatUrl) {
		      if(chatUrl.$value == null) {
		          var obj =   {
		                  "type" : "INDIVIDUAL",
		                  "memberIds" : [ 
		                      $scope.selectedUser
		                  ]
		                }  
		          chatService.intiateChat(obj,$rootScope.AUTHTOKEN).then(function(response) {
		               $rootScope.progressBarShow = false;

		               var obj ={
		                '$id':$scope.selectedUser,
		                'chat_id': response.id,

		               }  
		               $rootScope.afterIntiating =obj;
		                $scope.setTeamsHighlight();
		                $scope.backToChats();
		                $scope.setViewItem('teams');
		                $scope.setCurrentTeam($scope.selectedTeam);
		                $state.go('profile.productchats');
		            })
		        }
		        else {
		        	 $rootScope.progressBarShow = false;
		          	var obj ={
		                '$id':$scope.selectedUser,
		                'chat_id': chatUrl.$value,

		               }  
		                $rootScope.afterIntiating =obj;
		                $scope.setTeamsHighlight();
		                $scope.backToChats(); 

		                //utilityService.serviceError("chat with this person is already intiated ");
      				//return;
		        }
    });

    };
 });