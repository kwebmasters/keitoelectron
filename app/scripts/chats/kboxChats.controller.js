angular.module('keitoApp').controller('kboxChatsCtrl', function ($scope,Users,chatService,createTeamService,$rootScope,$uibModal,discussionService,$log,utilityService,$state,$firebaseArray) {
  
 $scope.utilityService = utilityService;
  $scope.userService = Users;
  $scope.chatService = chatService;
  $scope.getCountOfUnreadMsgs = function (id) {
  	return chatService.getCountOfUnreadMsgs($scope.loggedinuser.id,id)

  }
  
  $scope.chatsLoaded = false;

  $scope.chatsModal = function () {
    $scope.changeCreateDiscussion('kboxCreateChats');
    $state.go('profile.kboxCreateChats');
  }

  var chatMessagesRef = firebase.database().ref('node_chat_messages');

chatMessagesRef.on('child_changed', function(data) {
  $scope.loadChats();
});


  $scope.loadChats = function() {
      $rootScope.progressBarText1 = "Loading Contacts";
      $rootScope.progressBarText2 = "Loading Chats";
      $rootScope.progressBarText3 = "Loading Messages";
      $rootScope.progressBarShow = true;

      chatService.getAllChats($scope.loggedinuser.id).$loaded().then(function(allChatUsers) {
            
      		chatService.userDetailsRef.$loaded().then(function(Message) {
      			$scope.chatUsers = allChatUsers;
		      				$rootScope.progressBarShow = false;
		            	   
		            	 /*  $rootScope.unreadCount = 0;

			           for(var i=0;i<allChatUsers.length;i++) {
                      $rootScope.unreadCount = $rootScope.unreadCount + chatService.userDetailsRef.$getRecord($scope.loggedinuser.id).INDIVIDUAL_CHATS[allChatUsers[i].$id].unread_msg_count;
			            }
                console.log($rootScope.unreadCount);*/
		            $scope.chatsLoaded = true;


               /*   discussionService.getAllGroupChats($scope.loggedinuser.id).$loaded().then(function(groupChats) {
                   discussionService.getAllDiscussions($scope.loggedinuser.id).$loaded().then(function(discussions) {
                      chatService.userDetailsRef.$loaded().then(function() { 
                        $rootScope.progressBarShow = false;
                        $scope.allDiscussionUsers =[];
                        $scope.allDiscussionUsers = groupChats;
                        $scope.allDiscussionUsers.push.apply(groupChats,discussions);
                        for(var i=0;i< $scope.allDiscussionUsers.length;i++) {
                            if( $scope.allDiscussionUsers[i].chat_type == 'GROUP') {
                                  $rootScope.unreadCount = $rootScope.unreadCount + chatService.userDetailsRef.$getRecord($scope.loggedinuser.id).GROUP_CHATS[ $scope.allDiscussionUsers[i].$id].unread_msg_count;
                            }
                            else {
                                  $rootScope.unreadCount = $rootScope.unreadCount + chatService.userDetailsRef.$getRecord($scope.loggedinuser.id).DISCUSSIONS[ $scope.allDiscussionUsers[i].$id].unread_msg_count;
                            }

                        }
                    });
                  });

                });*/


              });
            
        });



      
    }

    $scope.loadChats();

    $scope.showChat =function(user) {

      createTeamService.getmyteam($rootScope.AUTHTOKEN).then(function(response) {
          
          var setOuterBreak = false;
          var teams =response;  
          for(var i=0;i<teams.length;i++) {
            for(var j=0;j<teams[i].members.length;j++){
              if(teams[i].members[j].userId == user.$id) {
                $rootScope.fromKbox = user;      
                $state.go('profile.productchats');
                $scope.producttab('Chats');
                $scope.setTeamsHighlight();
                $scope.setViewItem('teams');
                $scope.setCurrentTeam(teams[i].id);  
                setOuterBreak = true;
                break;
              }
              if(setOuterBreak) break;
            }
          }
               
      })
      
    }

 });


angular.module('keitoApp').directive('getTranslatedText', function ($http,Users,chatService,$rootScope,$firebaseArray) {
    return {
        transclude: true,
        scope: {
      user:'@user'
    },
    link: function(scope, element, attrs) {
    	
      scope.user = JSON.parse(scope.user);
   
	   if(scope.user.last_read_message_id != null || scope.user.last_read_message_id !=undefined) {
        var array = $firebaseArray(firebase.database().ref('node_chat_messages').child(scope.user.chat_id).orderByKey().startAt(scope.user.last_read_message_id));
        array.$loaded().then(function(Message) {
    			if(Message.length >1) {
    				scope.showRow = true;
    			}
    		});
	   }
    },
        template: '<ng-transclude ng-if="showRow"></ng-transclude>'
        
    };

});


angular.module('keitoApp').directive('haveUnread', function ($http,Users,chatService,$rootScope,$firebaseArray) {
    return {
        transclude: true,
        scope: {
      user: '@user',
    },
    link: function(scope, element, attrs) {
      scope.user = JSON.parse(scope.user);
      if(scope.user.unread_msg_count >0) {
        scope.boldClass="unreadChatClass";
      }
      else {
        scope.boldClass=""
      }
    },
    template: '<ng-transclude  class={{boldClass}}></ng-transclude>'
        
    };

});



angular.module('keitoApp').directive('unreadForteam', function ($http,Users,chatService,$rootScope,$firebaseArray) {
    return {
        transclude: true,
        scope: {
      team: '@team',
    },
    link: function(scope, element, attrs) {

      chatService.getChatsByTeamId(scope.team,$rootScope.AUTHTOKEN).then(function(response) {
          var teamChats = [];
          teamChats = response.INDIVIDUAL;
          scope.teamUnread =0;

          teamDiscussion = response.DISCUSSION;
          teamGroup = response.GROUP;
          
          if(teamChats!= undefined){ 
            for(var i=0;i<teamChats.length;i++) {
            for(var j=0;j<teamChats[i].members.length;j++) {
              if(teamChats[i].members[j].userId != $rootScope.loggedinuser.id){
                scope.teamUnread = scope.teamUnread + chatService.userDetailsRef.$getRecord($rootScope.loggedinuser.id).INDIVIDUAL_CHATS[teamChats[i].members[j].userId].unread_msg_count;
              }
              
            }
          }
        }

        if(teamDiscussion != undefined) {  for(var i=0;i<teamDiscussion.length;i++) {
            scope.teamUnread = scope.teamUnread + chatService.userDetailsRef.$getRecord($rootScope.loggedinuser.id).DISCUSSIONS[teamDiscussion[i].id].unread_msg_count;
          }
        }

        if(teamGroup != undefined) {  for(var i=0;i<teamGroup.length;i++) {
            scope.teamUnread = scope.teamUnread + chatService.userDetailsRef.$getRecord($rootScope.loggedinuser.id).GROUP_CHATS[teamGroup[i].id].unread_msg_count;
          }
        }


        });

    },
    template: '<ng-transclude  style="display: inline-block;padding: 2px 4px;border-radius: 30px;background: red;min-width: 20px;text-align: center;" ng-if="teamUnread >0">{{teamUnread }}</ng-transclude>'
        
    };

});

//ng-class=" { \'fa-angle-up\': !isCollapsed, \'fa-angle-down\': isCollapsed } "

