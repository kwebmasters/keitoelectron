angular.module('keitoApp').controller('productchatsCtrl', function ($scope,$stateParams,focus,Users,discussionService,createTeamService,$firebaseArray,chatService,$rootScope,$uibModal,$log,utilityService,$state) {
 
  $scope.utilityService = utilityService;
  $scope.userService = Users;
  $scope.chatService = chatService;
  $rootScope.currentTeam = $stateParams.id;

 $scope.handlePaste = function(event,name) {
      $rootScope.progressBarText1 = "Loading File";
      $rootScope.progressBarText2 = "Fetching Details";
      $rootScope.progressBarText3 = "Uploading File";
      $rootScope.progressBarShow = true;

    chatService.uploadFile(event,$rootScope.AUTHTOKEN).then(function(response) {
       var obj = {
              time_stamp:firebase.database.ServerValue.TIMESTAMP,
              message_text:name,
              sender_user_id : $scope.loggedinuser.id,
              message_type : "file",
              fileID : response.path,
              device : "chrome"
          }

        chatService.pushFile(obj,$scope.loggedinuser.id,$scope.prevUser.chat_id).then(function() {
               $rootScope.progressBarShow = false;

               firebase.database().ref('node_user').child($scope.prevUser.$id).child('INDIVIDUAL_CHATS').child($scope.loggedinuser.id).update({
                    unread_msg_count:userDetailsRef.$getRecord($scope.prevUser.$id).INDIVIDUAL_CHATS[$scope.loggedinuser.id].unread_msg_count+1
                });
                firebase.database().ref('node_user').child($scope.loggedinuser.id).child('INDIVIDUAL_CHATS').child($scope.prevUser.$id).update({
                    unread_msg_count:0
                });      
            }) 
      })
  }

  $scope.downloadFile =function (fileID) {
    chatService.downloadFile(fileID,$rootScope.AUTHTOKEN).then(function(response) {
      console.log(response);
      var contentType = "application/octet-stream";
      var urlCreator = window.URL;
      if (urlCreator) {
          var blob = new Blob([data], { type: contentType });
          var url = urlCreator.createObjectURL(blob);
          var a = document.createElement("a");
          document.body.appendChild(a);
          a.style = "display: none";
          a.href = url;
          a.download = "download.pdf"; //you may assign this value from header as well 
          a.click();
          window.URL.revokeObjectURL(url);
      }


    });
  }

        $scope.set_color = function (user,index) {
  if($scope.prevUser != undefined) {
    if ($scope.prevUser.$id == user.$id) {
      return { 'background-color':'#c0c0c0' }
    }
  };

  if(!$scope.$$listenerCount['teamUpdated']){
    $scope.$on("teamUpdated", function(event, reload) {
        $scope.loadChats();
    });
  }
}

$scope.image = null
$scope.imageFileName = ''



var chatMessagesRef = firebase.database().ref('node_chat_messages');
var chatUserRef = firebase.database().ref('node_user').child($rootScope.loggedinuser.id);

chatUserRef.on('child_changed', function(data) {
 //$rootScope.updateUnreadMessagesInkbox();
 $rootScope.updateUnreadMessagesForTeam();
   /*createTeamService.getmyteam($rootScope.AUTHTOKEN).then(function(response) {
          $rootScope.teamsForProfile  = [];
          $rootScope.teamsForProfile =response.data;         
      })*/

});


$scope.checkWhetherToday = function(epochDate) {
   var todaysDate = new Date();

   var epoch = epochDate/1000;
        var inputDate = new Date(0); // The 0 there is the key, which sets the date to the epoch
        inputDate.setUTCSeconds(epoch);

   if(inputDate.setHours(0,0,0,0) == todaysDate.setHours(0,0,0,0)) {
       return "showThisDateFormat";
    }
    else {
      return "DntshowThisDateFormat";
    }
}

$scope.checkWhetherNtToday = function(epochDate) {
   var todaysDate = new Date();

   var epoch = epochDate/1000;
        var inputDate = new Date(0); // The 0 there is the key, which sets the date to the epoch
        inputDate.setUTCSeconds(epoch);

  if(inputDate.setHours(0,0,0,0) != todaysDate.setHours(0,0,0,0)) {
       return "showThisDateFormat";
  }
  else {
    return "DntshowThisDateFormat";
  }

}







$scope.currentSelectedChatFlag = 0;

var teamChats = [];

$scope.showChatSetting = function() {
  $scope.showSettingsDiv = true;
  $scope.setViewSettingSViewItem('Chat Settings'); 
}
 $rootScope.$on('hideSettingEvent', function(e) {  
        $scope.showSettingsDiv = false;            
    });


$scope.getChatsByTeamId = function(chatUsers,teamChats) {
  var finalChats = [];
  for (var i = 0; i < teamChats.length; i++) {
    for (var j = 0; j < chatUsers.length; j++) {
      if(chatUsers[j].chat_id == teamChats[i].id){
        finalChats.push(chatUsers[j]);
        break;
      }    
    }
  }

  return finalChats;
}

$scope.onKeydownChild = function(evt) {
  switch(evt.keyCode) {
    case 13:if($scope.currentSelectedChatFlag != 0 && !$scope.smallFormat) { 
              $scope.showChat($scope.chatUsers[$scope.currentSelectedChatFlag-1],$scope.currentSelectedChatFlag-1);
              $scope.showSmallFormat(true);
              break;
            }
            break;

    case 38: if($scope.currentSelectedChatFlag != 0 && $scope.currentSelectedChatFlag!=1) {
                  $scope.currentSelectedChatFlag--;
               }
             break;

    case 40: if($scope.currentSelectedChatFlag != $scope.chatUsers.length) {
                  $scope.currentSelectedChatFlag++;
               }
             break;
  }

  if (evt.altKey && evt.keyCode == 67) {
        //console.log('alt+c');
        $scope.chatsModal();
    }
};

$scope.setRowcolor = function(index) {
  if(index == $scope.currentSelectedChatFlag-1) {
    return { 'background-color':'#c0c0c0' }; 
  }
}


 
 $scope.chatsModal = function () {
    $scope.changeCreateDiscussion('createChats');
    $state.go('profile.createChats');
  }


    $scope.loadChats = function() {
      $rootScope.progressBarText1 = "Loading Contacts";
      $rootScope.progressBarText2 = "Loading Chats";
      $rootScope.progressBarText3 = "Loading Messages";
      $rootScope.progressBarShow = true;
        
      

      chatService.getChatsByTeamId($rootScope.currentTeam,$rootScope.AUTHTOKEN).then(function(response) {
        var teamChats = [];
        teamChats = response.INDIVIDUAL;
        
        chatService.getAllChats($scope.loggedinuser.id).$loaded().then(function(allChatUsers) {
            

            $scope.chatUsers = [];
            $scope.chatUsers = $scope.getChatsByTeamId(allChatUsers,teamChats);
            chatService.userMessagesDetailsRef.$loaded().then(function(Message) {
              $rootScope.progressBarShow = false;
              $scope.loadChatUsers = true;

              $scope.showChat($scope.chatUsers[0],0);
              $scope.showSmallFormat(true);

              if($rootScope.afterIntiating != undefined) {
                $scope.showChat($rootScope.afterIntiating);
                $scope.showSmallFormat(true,$rootScope.afterIntiating);
                $rootScope.afterIntiating = undefined;
              }

              if($rootScope.fromContacts != undefined) {
              chatService.getUserDetails($scope.loggedinuser.id,$rootScope.fromContacts.userId).$loaded().then(function(obj) {
                for(var i=0;i<$scope.chatUsers.length;i++) {
                  if(obj.$id == $scope.chatUsers[i].$id) {
                    $scope.showChat($scope.chatUsers[i],i);
                    $scope.showSmallFormat(true)
                    $rootScope.fromContacts = undefined;
                  }
                }
              });
             }

             if($rootScope.fromKbox != undefined) {
              chatService.getUserDetails($scope.loggedinuser.id,$rootScope.fromKbox.$id).$loaded().then(function(obj) {
                for(var i=0;i<$scope.chatUsers.length;i++) {
                  if(obj.$id == $scope.chatUsers[i].$id) {
                    $scope.showChat($scope.chatUsers[i],i);
                    $scope.showSmallFormat(true)
                    $rootScope.fromKbox = undefined;
                  }
                }
              });
             }


            });
        });
      })

      
    }

    //$scope.loadChats();

    if($rootScope.fromContacts != undefined) {
    chatService.getChatHeadById($scope.loggedinuser.id,$rootScope.fromContacts.userId).$loaded().then(function(chatUrl) {
      if(chatUrl.chat_id == null) {
          var obj =   {
                  "type" : "INDIVIDUAL",
                  "memberIds" : [ 
                      $rootScope.fromContacts.userId
                  ]
                }  
          chatService.intiateChat(obj,$rootScope.AUTHTOKEN).then(function(response) {
               $rootScope.progressBarShow = false;
               $scope.loadChats();  

               var obj ={
                '$id':$rootScope.fromContacts.userId,
                'chat_id': response.id
               }    
               $scope.showChat(obj);
               $scope.showSmallFormat(true,obj)
               $rootScope.fromContacts = undefined;
            })
        }
        else {
          $scope.loadChats(); 
        }
    });
  }
  else {
    $scope.loadChats();
  }

  $scope.lastReadMessageNode = function() {
    chatService.getLastMessageNode($scope.prevUser.chat_id).$loaded().then(function(chatMessages) {
          if(chatMessages.length!= 0){
              chatService.updateLastReadNode($scope.loggedinuser.id,$scope.prevUser.$id,chatMessages[0].$id);

            }      
        });
  }

  $scope.showChat = function (user,index) {

    focus('focusChatsInput');
    $scope.sendingmessage = undefined;
    $scope.prevUser = user;
    $scope.currentSelectedChatFlag = index+1;
    chatService.getChatMessages($scope.prevUser.chat_id).$loaded().then(function(chatMessages) {
        $scope.chatMessages = chatMessages;
        //$scope.lastReadMessageNode();
        firebase.database().ref('node_user').child($scope.loggedinuser.id).child('INDIVIDUAL_CHATS').child($scope.prevUser.$id).update({
            unread_msg_count:0
        });
    });
  }

  $scope.getClass = function(x) {
    if(x== $rootScope.loggedinuser.id) {
      return "message-personal";
    }
 }

  $scope.showSmallFormat = function(format,user) {
    $scope.typedmessage = undefined;
    if(format) {
      $scope.smallFormat = true;
    }
    else {
      if($scope.prevUser!= undefined) {
        if($scope.prevUser.$id == user.$id) {
          $scope.prevUser = undefined;
          $scope.smallFormat = false;
        }
      }
    }
  }

   $scope.getClass = function(x) {
    if(x== $rootScope.loggedinuser.id) {
      return "message-personal";
    }
  }

  $scope.sendMessage = function(typedmessage) {

    if(typedmessage == null || typedmessage == undefined || typedmessage == '') {
      utilityService.serviceError("please enter message");
      return;
    }

    chatService.pushMessage(typedmessage,$scope.loggedinuser.id,$scope.prevUser.chat_id).then(function() {
       firebase.database().ref('node_user').child($scope.prevUser.$id).child('INDIVIDUAL_CHATS').child($scope.loggedinuser.id).update({
            unread_msg_count:userDetailsRef.$getRecord($scope.prevUser.$id).INDIVIDUAL_CHATS[$scope.loggedinuser.id].unread_msg_count+1
        });
        firebase.database().ref('node_user').child($scope.loggedinuser.id).child('INDIVIDUAL_CHATS').child($scope.prevUser.$id).update({
            unread_msg_count:0
        });      
    })
  }


  //-----------styles-------------

	chatGroupHeight();
	
	 userChattPlatform();
	imgCtrl();
	chat();
	starlike();
	chattScroll();
	//mainChat();
	chatScroll();
	$(window).load(function(){
		imgCtrl();
		chattScroll();
		
		});
	$(window).resize(function() {
	chatGroupHeight();
	
	 userChattPlatform();
     imgCtrl(); 
	 chattScroll(); 
    });
	
	
	
	
	function chatGroupHeight(){
		var windowWith =$(window).outerWidth();
	var navbarHeight = $('.navbar').outerHeight();
	var topHeadHeight = $scope.topHeadHeight;
	var chaticon = $('.my-type-chat-ctrl').outerHeight();
	var WindowHeight = $(window).outerHeight();
	
   $('.keto-group').css({"height":WindowHeight - navbarHeight - topHeadHeight});
	 $('.outer-area').css({"height":WindowHeight - navbarHeight - topHeadHeight});
	 $('.contact-list').css({"height":WindowHeight - navbarHeight - topHeadHeight-chaticon});
	//my-type-chat-ctrl
	
	if(windowWith < 960){
		$('.keto-group').css({"height":WindowHeight - topHeadHeight});
		}
	}
		function chattScroll(){
		  var thisHeight = $('.keto-group-users-holder').height();
			var thisChailHeight = $('.keto-group-users-holder').find('ul').height();
			//alert(thisHeight);alert(thisChailHeight);
			
			if(thisHeight < thisChailHeight ){
			$('.keto-group-users').find('.keto-group-users-holder').css({"overflow-y":"scroll"});
		
				} else {
					
					
					}
		}
		
		function userChattPlatform(){
			
	var windowWidth = $(window).outerWidth();
	var userInfo = $('.user-info').outerHeight();
	var chatPlatForm = $('.chat-platForm').outerHeight();
	var navbarHeight = $scope.topHeadHeight;
	var topHeadHeight = $('.keto-page-top').outerHeight();
	var chatPlatTop = $('.chat-plat-top').outerHeight();
	var userCtrl = $('.user-ctrl').outerHeight();
	var myTypeChatCtrl =$('.my-type-chat-ctrl').outerHeight();
	var WindowHeight = $(window).outerHeight();
	var USERCHATHeight = WindowHeight  - userInfo - userCtrl - myTypeChatCtrl  - 100
	//alert(USERCHATHeight);
    $('.user-chat').css({"height":USERCHATHeight});
		  if( windowWidth < 960 ){
			$('.user-chat').css({"height": chatPlatForm - userCtrl + topHeadHeight })  
			$('.chat-platForm').css({"left": - windowWidth});
			
			  }
		  $('.back-to-chat').click(function(){
			  $('.chat-platForm').toggleClass('chat-platForm-active');
			  
			  });
 $('.keto-group-users-holder').find('li').find('p').click(function(){
	 $('.chat-platForm').toggleClass('chat-platForm-active');
	 });
 
		}
		
		
		
		function starlike(){
		$('.star-like').each(function() {
        var thisOne = $(this);
   $(thisOne).find('i').click(function(){
	   var thisIco = $(this);
	   $(thisIco).toggleClass('favorite');
	   if(thisIco.hasClass('favorite')){
		   $(this).html('favorite')
		   }else{
			     $(this).html('favorite_border')
			   }
	   }); 
	    });
		}
/*	function mainChat(){	
$('.my-type-chat-ctrl').find('.my-type-chat-btn').find('button').click(function(){
var message = $(this).closest('.my-type-chat-ctrl').find('textarea').val();
if(message.length == 0 ){

} else {
	$('.chat-platForm').find('.user-chat').find('.user-chat-all-holder').append('<div class="col l12 m12 s12 other-chat my-chat  no-padding"><div class="col other-chat-content"><div class="col l12 m12 s12 no-padding">' + message + '</div></div></div>');
	$('.my-type-chat-ctrl').find('textarea').val('');
	}

});


}*/

function chatScroll(){
$('.my-type-chat-ctrl').find('.my-type-chat-btn').find('button').on('click',function (e) {
		$('.user-chat').scrollTop($('.user-chat-all-holder').height());
  });
  }
  
  
/*......................................................................................chat*/
	function chat(){
    $('.ket-chat').find('h5').click(function(){
		var $this = $(this);
		$(this).closest('.ket-chat').children('.ket-chat-single').toggle();
		imgCtrl()
		$(this).toggleClass('kto-expanded');
		
		
	//
		if ($(this).closest('.start-chat-content').find($this).hasClass("kto-expanded")) {
           $($this).find('i').html('remove')
		   $(this).closest('.ket-chat-aco').siblings().find('h5').find('i').html('add');
        } else {
            $(this).closest('.start-chat-content').find($this).find('i').html('add')
			
        }

		
		if ($(this).closest('.texas-row').find( $this ).hasClass("kto-expanded")) {
           $($this).find('i').html('remove')
		   $(this).closest('.ket-chat-aco').siblings().find('h5').find('i').html('keyboard_arrow_right');
        } else {
            $(this).closest('.texas-row').find($this).find('i').html('keyboard_arrow_right')	
        }
		
$(this).closest('.ket-chat-aco').siblings().find('.ket-chat-single').hide();
		});
		
		
		
		$('.ket-al-center').find('p').click(function(){
			var thisIcon = $(this);
			$(this).closest('.ket-chat-single').find('.ket-chat-text').toggle();
			$(this).toggleClass('kto-expanded');
			if(thisIcon.hasClass("kto-expanded")){
				 $(thisIcon).find('i').html('remove')
				}else {
					 $(thisIcon).find('i').html('keyboard_arrow_right')
					}
			});
		
		}
/*......................................................................................chat*/		
/*......................................................................................img Ctrl*/
		function imgCtrl(){
		 $('.hight-ctrl, .chat-plat-top-pro, .other-chat-pro').each(function() {
        var thisDataHeight = $(this).data('height');
		var thisWidth = $(this).outerWidth();
		var convertHeight = (thisWidth * thisDataHeight)/100
		$(this).css({"height":convertHeight});	
		var thisImgHolder = $(this).find('.img-adj');
		var thisImg = $(thisImgHolder).find('img').attr('src');
		$(thisImgHolder).css({"background":'url('+ thisImg  +')'})
    });
	}
/*......................................................................................img Ctrl*/


 // $('.outer-area').css({"height":$scope.outerHeight});

});


angular.module('keitoApp').filter('notAccepted', function($rootScope) {
    return function(input) {
        var out = [];
        if(input != undefined) {
          for (var i = 0; i < input.length; i++) {
            if(!input[i].isJoined){
                out.push(input[i]);
            }
          }
        }

        
        return out;
    }
});

angular.module('keitoApp').filter('currentUser', function($rootScope) {
    //$rootScope.loggedinuser = "9022171147";

    return function(input) {
        var out = [];
        if(input != undefined) {
          for (var i = 0; i < input.length; i++) {
            if(input[i].mobileNumber != $rootScope.loggedinuser.mobileNumber){
                out.push(input[i]);
            }
          }
        }

        
        return out;
    }
});

angular.module('keitoApp').filter('removeLastTwo', function($rootScope) {
    //$rootScope.loggedinuser = "9022171147";

    return function(input) {
        var out = [];
        if(input != undefined) {
          for (var i = 0; i < input.length-2; i++) {
            if(input[i].mobileNumber != $rootScope.loggedinuser.mobileNumber){
                out.push(input[i]);
            }
          }
        }

        
        return out;
    }
});

angular.module('keitoApp').filter('onlyFirstName', function($rootScope) {
    //$rootScope.loggedinuser = "9022171147";

    return function(input) {
        if(input != undefined && input.indexOf(' ') != -1) {
          return input.substr(0,input.indexOf(' '))
        } 
        else
          return input;       
          
    }
});


angular.module('keitoApp').directive('scroll', function($timeout) {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {
      scope.$watchCollection(attr.scroll, function(newVal) {
        $timeout(function() {
         element[0].scrollTop = element[0].scrollHeight-5;
        });
      });
    }
  }
});

angular.module('keitoApp').directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.$apply(function(){
                        scope.$eval(attrs.ngEnter, {'event': event});
                    });

                    event.preventDefault();
                }
            });
        };
});

angular.module('keitoApp').directive(
            "bnDocumentClick",
            function( $document, $parse ){
                // I connect the Angular context to the DOM events.
                var linkFunction = function( $scope, $element, $attributes ){
                    // Get the expression we want to evaluate on the
                    // scope when the document is clicked.
                    var scopeExpression = $attributes.bnDocumentClick;
                    // Compile the scope expression so that we can
                    // explicitly invoke it with a map of local
                    // variables. We need this to pass-through the
                    // click event.
                    //
                    // NOTE: I ** think ** this is similar to
                    // JavaScript's apply() method, except using a
                    // set of named variables instead of an array.
                    var invoker = $parse( scopeExpression );
                    // Bind to the document click event.
                  
                    $document.on(
                        "keydown keypress",
                        function( event ){
                            // When the click event is fired, we need
                            // to invoke the AngularJS context again.
                            // As such, let's use the $apply() to make
                            // sure the $digest() method is called
                            // behind the scenes.
                            $scope.$apply(
                                function(){
                                    // Invoke the handler on the scope,
                                    // mapping the jQuery event to the
                                    // $event object.
                                    invoker(
                                        $scope,
                                        {
                                            $event: event
                                        }
                                    );
                                }
                            );
                        }
                    );
                    // TODO: Listen for "$destroy" event to remove
                    // the event binding when the parent controller
                    // is removed from the rendered document.
                };
                // Return the linking function.
                return( linkFunction );
            }
        );

angular.module('keitoApp').directive('fileDropzone', function() {
    return {
      restrict: 'A',
      scope: {
        file: '=',
        fileName: '=',
        onloaded: '&'
      },
      link: function(scope, element, attrs) {
        var checkSize, isTypeValid, processDragOverOrEnter, validMimeTypes;
        processDragOverOrEnter = function(event) {
          if (event != null) {
            event.stopPropagation();
            event.preventDefault();

          }
          obj = {
              "effectAllowed" : "copy"
          }

          event["dataTransfer"] = obj;
          return false;
        };
        validMimeTypes = attrs.fileDropzone;
       
        element.bind('dragover', processDragOverOrEnter);
        element.bind('dragenter', processDragOverOrEnter);

        return element.bind('drop', function(event) {
          var file, name, reader, size, type;
          if (event != null) {
            event.stopPropagation();
            event.preventDefault();
          }
          
          file = event.originalEvent.dataTransfer.files[0];
          var formData = new FormData();
            formData.append("file", file);

             scope.onloaded({ "image":formData , "name" :file.name});

             
          return false;
        });
      }
    };
  });
