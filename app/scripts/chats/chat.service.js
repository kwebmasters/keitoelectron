angular.module('keitoApp').factory('chatService', ['$firebaseArray', '$firebaseObject', '$http','$q','utilityService',
  function($firebaseArray,$firebaseObject,$http,$q,utilityService) {
    var usersRef = firebase.database().ref('node_user');

    var userMessagesRef = firebase.database().ref('node_chat');

    var chatMessagesRef = firebase.database().ref('node_chat_messages');

    var chat_url = "http://test-apis.keito.in/chat";
    var upload_file = "http://test-apis.keito.in/file";

    userMessagesDetailsRef = $firebaseArray(userMessagesRef);
    userDetailsRef = $firebaseArray(usersRef);


    var chatServices =  {
      userMessagesDetailsRef : $firebaseArray(userMessagesRef),
      userDetailsRef : $firebaseArray(usersRef),
      chatMessagesDetailsRef : $firebaseArray(chatMessagesRef),
      intiateChat: function(data,token){
          var deferred = $q.defer();
           $http({
                      method: "POST",
                      headers: {'Content-Type': 'application/json;','AUTH-TOKEN':token},
                      url:chat_url,
                      data: data
                  })
             .success(function(data) { 
                deferred.resolve(data);

             }).error(function(msg, code) {
                deferred.reject(msg);
                utilityService.serviceError(msg.errors[0]);
             });
           return deferred.promise;
      },

      getChatsByTeamId: function(teamId,token){
          var deferred = $q.defer();
           $http({
                      method: "GET",
                      headers: {'Content-Type': 'application/json;','AUTH-TOKEN':token},
                      url:"http://test-apis.keito.in/team/"+teamId+"/chats"
                  })
             .success(function(data) { 
                deferred.resolve(data);

             }).error(function(msg, code) {
                deferred.reject(msg);
                utilityService.serviceError(msg.errors[0]);
             });
           return deferred.promise;
      },

      getUserFile: function(token){
          var deferred = $q.defer();
           $http({
                      method: "GET",
                      headers: {'Content-Type': 'application/json;','AUTH-TOKEN':token},
                      url:upload_file
                  })
             .success(function(data) { 
                deferred.resolve(data);

             }).error(function(msg, code) {
                deferred.reject(msg);
                utilityService.serviceError(msg.errors[0]);
             });
           return deferred.promise;
      },

       uploadFile: function(data,token){
          var deferred = $q.defer();
           $http({
                      method: "POST",
                      headers: {'Content-Type': undefined,'AUTH-TOKEN':token , 'enctype': 'multipart/form-data' },
                      url:upload_file,
                      data: data
                  })
             .success(function(data) { 
                deferred.resolve(data);

             }).error(function(msg, code) {
                deferred.reject(msg);
                utilityService.serviceError(msg.errors[0]);
             });
           return deferred.promise;
      },

      downloadFile: function(fileID,token){
          var deferred = $q.defer();
           $http({
                      method: "GET",
                      headers: {'Content-Type': undefined,'AUTH-TOKEN':token},
                      url: upload_file+'/'+fileID
                  })
             .success(function(data) {
                 
                deferred.resolve(data);

             }).error(function(msg, code) {
                deferred.reject(msg);
                utilityService.serviceError(msg.errors[0]);
             });
           return deferred.promise;
      },

      getChatHeadById: function(uid1,uid2){
        var path =  uid1+'/INDIVIDUAL_CHATS/'+uid2 ;

        return $firebaseObject(usersRef.child(path));
      },
      getAllChats: function(uid1){
        var path =  uid1+'/INDIVIDUAL_CHATS/';

        return $firebaseArray(usersRef.child(path));
      },
      getLastMessage: function(url){
        

        if(userMessagesDetailsRef.$getRecord(url) == undefined || userMessagesDetailsRef.$getRecord(url)== null) {
          return
        }
        if(userMessagesDetailsRef.$getRecord(url).last_message == undefined || userMessagesDetailsRef.$getRecord(url).last_message == null) {
            return ;
          } 
          
        return userMessagesDetailsRef.$getRecord(url).last_message.message_text;

        //return $firebaseObject(usersRef.child(path));
      },
      getLastSender: function(url){
        

        if(userMessagesDetailsRef.$getRecord(url) == undefined || userMessagesDetailsRef.$getRecord(url)== null) {
          return
        }
        if(userMessagesDetailsRef.$getRecord(url).last_message == undefined || userMessagesDetailsRef.$getRecord(url).last_message == null) {
            return ;
          } 
          
        return userMessagesDetailsRef.$getRecord(url).last_message.sender_user_id;

        //return $firebaseObject(usersRef.child(path));
      },

      getlastTimestamp: function(url){
        

        if(userMessagesDetailsRef.$getRecord(url) == undefined || userMessagesDetailsRef.$getRecord(url)== null) {
            return ;
        }

        if(userMessagesDetailsRef.$getRecord(url).last_message == undefined || userMessagesDetailsRef.$getRecord(url).last_message == null) {
            return ;
          } 

        return userMessagesDetailsRef.$getRecord(url).last_message.time_stamp;

        //return $firebaseObject(usersRef.child(path));
      },

      
      pushFile: function(obj,sender,url){
        var deferred = $q.defer();
        
        firebase.database().ref('node_chat_messages').child(url).push(obj,function (err) {
              deferred.resolve();
          });

        firebase.database().ref('node_chat').child(url).child('last_message').set(obj); 
        return deferred.promise;
      },

       pushMessage: function(message,sender,url){
        var deferred = $q.defer();
        var obj = {
              time_stamp:firebase.database.ServerValue.TIMESTAMP,
              message_text:message,
              sender_user_id :sender,
              message_type : "text",
              device : "chrome"
          }
        firebase.database().ref('node_chat_messages').child(url).push(obj,function (err) {
              deferred.resolve();
          });

        firebase.database().ref('node_chat').child(url).child('last_message').set(obj); 
        return deferred.promise;
      },

      getChatMessages: function(url){
       // var path =  url+'/messages' ;

        return $firebaseArray(chatMessagesRef.child(url));
      },

      getLastMessageNode: function(url){
        return $firebaseArray(firebase.database().ref('node_chat_messages').child(url).limitToLast(1));
      },
      getCountOfUnreadMsgs: function(uid1,uid2){
       //var deferred = $q.defer();
        var lastReadUrl = this.userDetailsRef.$getRecord(uid1).INDIVIDUAL_CHATS[uid2].last_read_message_id;
        var chat_id = this.userDetailsRef.$getRecord(uid1).INDIVIDUAL_CHATS[uid2].chat_id;
            
        if(lastReadUrl != null || lastReadUrl !=undefined) {
          var array = $firebaseArray(firebase.database().ref('node_chat_messages').child(chat_id).orderByKey().startAt(lastReadUrl));
            array.$loaded().then(function(Message) {
              if(Message.length >1) {
                return true;
              }
              else {
                return false;
              }
              
            });
        }
        else {
          return false;
        }
      },


      forUsers: function(){
        return $firebaseArray(usersRef);
      },
      forMessages: function(uid1,uid2){
        var path =  uid1+'/'+uid2 ;

        return userMessagesRef.child(path);
      },
      createChat : function(uid1,uid2,url,Lastmessage) {
        firebase.database().ref('node_user').child(uid1).child('chats').child(uid2).set(
          {
              LastTimestamp:firebase.database.ServerValue.TIMESTAMP,
              Lastmessage:Lastmessage,
              Url:url,
              isGroupChat: false
          });
      },
      forChats: function(uid1){
        var path =  uid1+'/chats';

        return $firebaseArray(usersRef.child(path));
      },     
      forChatById: function(uid1,uid2){
        var path =  uid1+'/chats/'+uid2 ;

        return $firebaseArray(usersRef.child(path));
      },
      updateLastReadNode :function(uid1,uid2,chat_id){
        firebase.database().ref('node_user').child(uid1).child('INDIVIDUAL_CHATS').child(uid2).update(
          {
              last_read_message_id:chat_id,
          });
      },

      sendMessages: function(url,message,sender){
        firebase.database().ref('node_chat').child(url).push(
          {
              timeStamp:firebase.database.ServerValue.TIMESTAMP,
              message:message,
              user_id:sender
          });
      },
      getUserDetails: function(uid1,uid2){
        var path =  uid1+'/chats/'+uid2 ;

        return $firebaseObject(usersRef.child(path));
      },
      forMessageDetails: function(url) {
        return $firebaseArray(userMessagesRef.child(url));
      }

    };

    return chatServices;

    }
  ]);