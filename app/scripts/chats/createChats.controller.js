angular.module('keitoApp').controller('createChatsCtrl', function ($scope,$state,Users,chatService,discussionService,$rootScope,$uibModal,$log,utilityService,createTeamService) {

     $rootScope.progressBarText1 = "Loading Teams";
    $rootScope.progressBarText2 = "Loading Teams Details";
    $rootScope.progressBarText3 = "Loading Teams Members";
    $rootScope.progressBarShow = true;

    createTeamService.getTeamById($rootScope.AUTHTOKEN,$rootScope.currentTeam).then(function(response) {
        
        $rootScope.teamForCreateChats = response;

        $rootScope.progressBarShow = false;
    })
    $scope.radioBtnChanged = function(user) {
    	$scope.selectedUser = user;
    	$scope.intiateChat();
    };

    $scope.intiateChat = function() {
    	console.log($scope.selectedUser);

    	$rootScope.progressBarText1 = "Loading Chats";
    	$rootScope.progressBarText2 = "Intiating New Chat";
    	$rootScope.progressBarText3 = "Getting Chat Details";
    	$rootScope.progressBarShow = true;


    	chatService.getChatHeadById($scope.loggedinuser.id,$scope.selectedUser).$loaded().then(function(chatUrl) {
		      if(chatUrl.$value == null) {
		          var obj =   {
		                  "type" : "INDIVIDUAL",
		                  "memberIds" : [ 
		                      $scope.selectedUser
		                  ]
		                }  
		          chatService.intiateChat(obj,$rootScope.AUTHTOKEN).then(function(response) {
		               $rootScope.progressBarShow = false;
		               var obj ={
		                '$id':$scope.selectedUser,
		                'chat_id': response.id,

		               }  
		               $rootScope.afterIntiating =obj;
		                $scope.backToChats(); 
		            })
		        }
		        else {
		        	 $rootScope.progressBarShow = false;
		          	var obj ={
		                '$id':$scope.selectedUser,
		                'chat_id': chatUrl.$value,

		               }  
		               $rootScope.afterIntiating =obj;
		                $scope.backToChats(); 
		        }
    	});

    };
 });