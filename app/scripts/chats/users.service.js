angular.module('keitoApp')
  .factory('Users', function($firebaseArray, $firebaseObject){

    var usersRef = firebase.database().ref('node_user');
    var users = $firebaseArray(usersRef);
    var connectedRef = firebase.database().ref('.info/connected');
    var profileImageRef = firebase.database().ref('ProfileImages')
    var profileImageArray = $firebaseArray(profileImageRef);
    var Users = {

    getRecord : function(uid) {
      return usersRef.$getRecord(uid)
    },  
    getProfile: function(uid){
       if(profileImageArray.$getRecord(uid) == undefined || profileImageArray.$getRecord(uid)== null) {
          return
        }
      return profileImageArray.$getRecord(uid).image;
    },
    getShowProfilePicture :function(uid) {
     if(users.$getRecord(uid) == undefined || users.$getRecord(uid)== null) {
          return
        }
      return users.$getRecord(uid).showProfilePicture;
    },
    getTeamProfileSettings: function(uid){
      if(users.$getRecord(uid) == undefined || users.$getRecord(uid)== null) {
          return
        }
      return users.$getRecord(uid).teamProfileSettings;
    },
    getDisplayName: function(uid){
      if(users.$getRecord(uid) == undefined || users.$getRecord(uid)== null) {
          return
        }
      return users.$getRecord(uid).name;
    },
    getStatus: function(uid){
      if(users.$getRecord(uid) == undefined || users.$getRecord(uid)== null) {
          return
        }

      return users.$getRecord(uid).status;
    },
    getMobileNumber: function(uid){
      if(users.$getRecord(uid) == undefined || users.$getRecord(uid)== null) {
          return
        }
     return users.$getRecord(uid).mobileNumber;
    },
    getGravatar: function(uid){
    return '//www.gravatar.com/avatar/' + users.$getRecord(uid).emailHash;
    },
    getOnline: function(uid){
      if(users.$getRecord(uid) == undefined || users.$getRecord(uid)== null) {
          return
        }
      if(!users.$getRecord(uid).online) {
        return
      }
      var keys = Object.keys(users.$getRecord(uid).online)
      
      return users.$getRecord(uid).online;
    },
    setOnline: function(uid){
      var connected = $firebaseObject(connectedRef);
      var online = $firebaseArray(usersRef.child(uid+'/online'));

      connected.$watch(function (){
        if(connected.$value === true){
          online.$add(true).then(function(connectedRef){
            connectedRef.onDisconnect().remove();
          });
        }
      });
    },
    all: users
  };

    return Users;
  });