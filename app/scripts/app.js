'use strict';


var config = {
  apiKey: "AIzaSyAZ2RCz2spLl58mRIRYmz-mFGfrcwBNGQg",
  authDomain: "keito-dev.firebaseapp.com",
  databaseURL: "https://keito-dev.firebaseio.com",
  projectId: "keito-dev",
  storageBucket: "keito-dev.appspot.com",
  messagingSenderId: "50934727207"
};
firebase.initializeApp(config);

angular
  .module('keitoApp', [

    'ui.router', 'ui.toggle', 'ngSanitize', 'ui.bootstrap', 'ngRoute', 'firebase', 'angular.chips',
    'angular-smilies'
  ])
  .run(['$rootScope', '$state',
    function ($rootScope, $state) {
      $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        if (toState.data) {
          var requireLogin = toState.data.requireLogin;

          if (requireLogin && typeof $rootScope.loggedinuser === 'undefined') {
            event.preventDefault();
            $state.go('login');
          }
        }
      });
    }]
  )
  .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .state('home', {
        url: '/home',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .state('product', {
        url: '/product',
        templateUrl: 'views/product.html',
        controller: 'productCtrl'
      })
      .state('industries', {
        url: '/industries',
        templateUrl: 'views/industries.html',
        controller: 'industriesCtrl'
      })
      .state('ourTeam', {
        url: '/ourteam',
        templateUrl: 'views/ourTeam.html',
        controller: 'footerCtrl'
      })
      .state('blog', {
        url: '/blog',
        templateUrl: 'views/blog.html',
        controller: 'footerCtrl'
      })
      .state('security', {
        url: '/security',
        templateUrl: 'views/security.html',
        controller: 'footerCtrl'
      })
      .state('references', {
        url: '/references',
        templateUrl: 'views/references.html',
        controller: 'footerCtrl'
      })
      .state('termsandconditions', {
        url: '/termsandconditions',
        templateUrl: 'views/termsandconditions.html',
        controller: 'footerCtrl'
      })
      .state('privacypolicy', {
        url: '/privacypolicy',
        templateUrl: 'views/privacypolicy.html',
        controller: 'footerCtrl'
      })

      .state('joinNow', {
        url: '/joinNow',
        templateUrl: 'views/joinNow/sign-up.html',
        controller: 'signUpCtrl'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'views/login/sign-in.html',
        controller: 'signInCtrl'
      })
      .state('loginOtp', {
        url: '/login',
        params: {
          "otp": null,
          "mobile": null,
        },
        templateUrl: 'views/login/otp.html',
        controller: 'signInOtpCtrl'
      })
      .state('pricing', {
        url: '/pricing',
        templateUrl: 'views/pricing.html',
        controller: 'pricingCtrl'
      })
      .state('support', {
        url: '/support',
        templateUrl: 'views/support.html',
        controller: 'productCtrl'
      })
      .state('contactUs', {
        url: '/contactUs',
        templateUrl: 'views/contactUs.html',
        controller: 'pricingCtrl'
      })
      .state('profile', {
        url: '/profile',
        templateUrl: 'views/home/home.html',
        controller: 'profileCtrl',
        abstract: true,
        data: {
          requireLogin: true
        }
      })
      .state('profile.kbox', {
        url: '/kbox',
        templateUrl: "views/kbox/kbox.html",
        abstract: true,
        data: {
          requireLogin: true
        }
      })
      .state('profile.kbox.chats', {
        url: '/kbox',
        templateUrl: "views/chats.html"
      })
      .state('profile.kbox.discussions', {
        url: '/kbox',
        templateUrl: "views/discussions.html"
      })
      .state('profile.kbox.profile', {
        url: '/kbox',
        templateUrl: "views/myprofile.html"
      })
      .state('profile.manageTeam', {
        url: '/manageTeam',
        templateUrl: "views/manageTeam/manage-team.html",
        controller: 'createTeamCtrl'
      })
      .state('profile.manageTeam.createNewTeam', {
        templateUrl: "views/manageTeam/create-team-new.html",
        controller: 'createTeamCtrl'
      })
      .state('profile.team', {
        url: '/team',
        templateUrl: "views/teams/team.html",
        controller: "teamsCtrl",
        abstract: true,
        data: {
          requireLogin: true
        }
      })
      .state('profile.team.chats', {
        url: '/team',
        templateUrl: "views/teams/chats.html",
        controller: "productchatsCtrl",
        data: {
          requireLogin: true
        },
        params: {
          'id': 0
        }
      })
      .state('profile.team.contacts', {
        url: '/contacts',
        templateUrl: "views/contacts/contacts.html",
        controller: "productcontactsCtrl",
        data: {
          requireLogin: true
        }
      })
    /*.state('profile.kbox', {
      url:'/kboxCreateDiscussoins',
            templateUrl: "views/kboxCreateDiscussion.html",
            controller :'kboxCreateDiscussionCtrl'
    })*/
    /*.state('profile.chats', {
      url:'/chats',
      templateUrl: "views/chats.html",
      params: {
          "AUTHTOKEN": null
      },
      controller: 'kboxChatsCtrl'
    })
    .state('profile.discussions', {
      url:'/discussions',
            templateUrl: "views/discussions.html",
            controller: 'kboxDiscussionsCtrl'
    })
    .state('profile.myprofile', {
      url:'/myprofile',
            templateUrl: "views/myprofile.html",
            controller: 'myProfileCtrl'
    })
    .state('profile.productchats', {
      url:'/productchats',
            templateUrl: "views/productchats.html",
            controller: 'productchatsCtrl'
    })
    .state('profile.productcontacts', {
      url:'/productcontacts',
            templateUrl: "views/productcontacts.html",
            controller: 'productcontactsCtrl'
    })
    .state('profile.productdiscussions', {
      url:'/productdiscussions',
            templateUrl: "views/productdiscussions.html",
            controller: 'productdiscussionsCtrl'
    })
    .state('profile.productmyprofile', {
      url:'/productmyprofile',
            templateUrl: "views/productmyprofile.html",
            controller: 'productmyprofileCtrl'
    })
    .state('profile.createteam', {
      url:'/createteam',
            templateUrl: "views/createteam.html",
            controller :'createTeamCtrl'
    })
    .state('profile.createDiscussion', {
      url:'/createDiscussion',
            templateUrl: "views/createDiscussion.html",
            controller :'createDiscussionCtrl'
    })
    .state('profile.createChats', {
      url:'/createChats',
            templateUrl: "views/createChats.html",
            controller :'createChatsCtrl'
    })
    .state('profile.kboxCreateChats', {
      url:'/kboxCreateChats',
            templateUrl: "views/kboxCreateChats.html",
            controller :'kboxCreateChatsCtrl'
    })

    .state('profile.kboxCreateDiscussoins', {
      url:'/kboxCreateDiscussoins',
            templateUrl: "views/kboxCreateDiscussion.html",
            controller :'kboxCreateDiscussionCtrl'
    });*/
    $urlRouterProvider.otherwise('/');
    //$httpProvider.defaults.headers.common = {};
    //$httpProvider.defaults.headers.post = { 'Content-Type': 'application/json'};
    //$httpProvider.defaults.headers.put = {};
    //$httpProvider.defaults.headers.patch = {};
  });




