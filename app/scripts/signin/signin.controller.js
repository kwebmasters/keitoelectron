angular.module('keitoApp').controller('signInCtrl', function ($scope,signInService,$rootScope,$http,$state,utilityService) {
    
    document.getElementById('progressBarShowLoading').style.display = "none";
    document.getElementById('afterLoading').style.display = "";
    
    $rootScope.progressBarShow = false;
    $rootScope.progressBarShowLoading = false;
    $rootScope.mbody = "sbody";
    $rootScope.footerclr = "";
    $rootScope.footerhide = true;

    var emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    var mobileRegx= /^([7-9][0-9]*)$/

    $scope.forgotPassword =  function() {
      if($scope.email == undefined || $scope.email == null){
            utilityService.serviceError("Please Enter Email Id");
            return;
          }

      if(!emailRegx.test($scope.email)) {
          utilityService.serviceError("Please Enter Valid Email Id");
            return;
      }
      var obj = {
        "email" : $scope.email
      }

      $rootScope.progressBarText1 = "Loading";
      $rootScope.progressBarText2 = "Fetching Data";
      $rootScope.progressBarText3 = "Verifying email";
      $rootScope.progressBarShow = true;

      signInService.forgotPassword(obj).then(function(response) {
        $rootScope.progressBarShow = false;
        utilityService.serviceError("Password sent to mail successfully");
            return;
      })

    };

    $scope.pleaseLogin = function() {
      //utilityService.serviceError('Please login through OTP');
      var emailCheck = false;
      if($scope.email == undefined || $scope.email == null){
            utilityService.serviceError("Please Enter  Email Id or Mobile No");
            return;
          }

      if(!emailRegx.test($scope.email)) {
            if($scope.email.length !=10 || !mobileRegx.test($scope.email)) {
            utilityService.serviceError("Please Enter Valid Mobile No. or Email Id");
            return;
          }
          //utilityService.serviceError("Please Enter Valid Email Id.");
          //return;
      }
      else {
        emailCheck = true;
      }


      if($scope.password == undefined || $scope.password == null){
            utilityService.serviceError("Please Enter Password");
            return;
          }

      var obj = {
        "email" : $scope.email,
        "password" : $scope.password
      }

      if(!emailCheck) {
        obj = {
           "mobileNumber" : $scope.email,
           "password" : $scope.password
        }
      }

      $rootScope.progressBarText1 = "Loading";
      $rootScope.progressBarText2 = "Handling Data";
      $rootScope.progressBarText3 = "Verifying Password";
      $rootScope.progressBarShow = true;
      signInService.verifyPassword(obj).then(function(response) {
        $rootScope.progressBarShow = false;
         $rootScope.AUTHTOKEN = response["AUTH-TOKEN"];

         firebase.auth().signInWithCustomToken($rootScope.AUTHTOKEN).then(function (auth){
             
         }).catch(function(error) {
          
        });

          $rootScope.loggedinuser = response.user;
          $scope.AUTHTOKEN = response["AUTH-TOKEN"];
          $state.go('profile.chats', { "AUTHTOKEN" : $scope.AUTHTOKEN});
        
      })

    };

    //$scope.email = '8374555555';
    $scope.sendOtp = function() {
      if($scope.email == undefined || $scope.email == null){
            utilityService.serviceError("Please Enter Mobile No.");
            return;
          }

      if(!emailRegx.test($scope.email)) {
            if($scope.email.length !=10 || !mobileRegx.test($scope.email)) {
            utilityService.serviceError("Please Enter Valid Mobile No. or Email Id");
            return;
          }
          //utilityService.serviceError("Please Enter Valid Email Id.");
          //return;
      }

      var obj = {
        "mobileNumber" : $scope.email
      }

      $rootScope.progressBarText1 = "Loading";
      $rootScope.progressBarText2 = "Handling Data";
      $rootScope.progressBarText3 = "Sending OTP";
      $rootScope.progressBarShow = true;
      signInService.sendOtp(obj).then(function(response) {
        $rootScope.progressBarShow = false;
        $scope.otp = response.otp;
        $state.go('loginOtp', { "otp" : $scope.otp, "mobile": $scope.email});
        
      })
    }    
});

angular.module('keitoApp').controller('signInOtpCtrl', function ($scope,signInService,$rootScope,$http,$state,$stateParams,Users) {
  $rootScope.mbody = "sbody";
    $rootScope.footerclr = "";
    $rootScope.footerhide = true;
    $scope.otp = $stateParams.otp

    var emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    var mobileRegx= /^([7-9][0-9]*)$/

    $scope.resend = function() {
      var obj = {
        "mobileNumber" : $stateParams.mobile
      }
      $rootScope.progressBarShow = true;
      signInService.sendOtp(obj).then(function(response) {
        $rootScope.progressBarShow = false;
        $scope.otp = response.otp;
      })
    }

    $scope.verify = function() {
       var obj = {
        "mobileNumber" : $stateParams.mobile,
        "otp" : $scope.otp
      }

      $rootScope.progressBarText1 = "Setting up Keito";
      $rootScope.progressBarText2 = "Loading K-Box";
      $rootScope.progressBarText3 = "Retrieving files and chats";
      $rootScope.progressBarShow = true;

      signInService.verifyOtp(obj).then(function(response) {
         $rootScope.progressBarShow = false;
         $rootScope.AUTHTOKEN = response["AUTH-TOKEN"];

         firebase.auth().signInWithCustomToken($rootScope.AUTHTOKEN).then(function (auth){
             
         }).catch(function(error) {
          
        });

          $rootScope.loggedinuser = response.user;
          Users.setOnline($rootScope.loggedinuser.id);
          $scope.AUTHTOKEN = response["AUTH-TOKEN"];
          $state.go('profile.kbox.chats', { "AUTHTOKEN" : $scope.AUTHTOKEN});
      });
    }
   
});
