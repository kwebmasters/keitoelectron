angular.module('keitoApp').factory('signInService', [ '$http','$q', '$log', 'utilityService', '$rootScope',
  function($http,$q,$log,utilityService,$rootScope) {
    //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";


    var otp_url = "http://test-apis.keito.in/user/generate_otp";
    var verify_url = "http://test-apis.keito.in/user/validate_otp"; 
    var verify_password = "http://test-apis.keito.in/user/sign_in";
    var resend_url = "http://test-apis.keito.in/user/resend_verification_email";
    var forgot_password_url = "http://test-apis.keito.in/user/forgot_password";
    var update_password_url = "http://test-apis.keito.in/user/update_password"
    var update_user_url = "http://test-apis.keito.in/user"

     return {   
      sendOtp: function (data){

              var deferred = $q.defer();
               $http({
                          method: "PUT",
                          headers: {'Content-Type': 'application/json;'},
                          url:otp_url,
                          data: data
                      })
                 .success(function(data) { 
                    deferred.resolve(data);

                 }).error(function(msg, code) {
                    deferred.reject(msg);
                    $rootScope.progressBarShow = false;
                    utilityService.serviceError(msg.errors[0]);
                 });
               return deferred.promise;

              
              },
     updateUser : function (data,token){

              var deferred = $q.defer();
               $http({
                          method: "PUT",
                          headers: {'Content-Type': 'application/json;','AUTH-TOKEN':token},
                          url:update_user_url,
                          data: data
                      })
                 .success(function(data) { 
                    deferred.resolve(data);

                 }).error(function(msg, code) {
                    deferred.reject(msg);
                    $rootScope.progressBarShow = false;
                    utilityService.serviceError(msg.errors[0]);
                 });
               return deferred.promise;

              
              },
     updatePassword: function (data,token){

              var deferred = $q.defer();
               $http({
                          method: "PUT",
                          headers: {'Content-Type': 'application/json;','AUTH-TOKEN':token},
                          url:update_password_url,
                          data: data
                      })
                 .success(function(data) { 
                    deferred.resolve(data);

                 }).error(function(msg, code) {
                    deferred.reject(msg);
                    $rootScope.progressBarShow = false;
                    utilityService.serviceError(msg.errors[0]);
                 });
               return deferred.promise;

              
            },

      forgotPassword : function(data) {

        var deferred = $q.defer();
                 $http({
                            method: "POST",
                            headers: {'Content-Type': 'application/json;'},
                            url:forgot_password_url,
                            data: data
                        })
                   .success(function(data) { 
                      deferred.resolve(data);

                   }).error(function(msg, code) {
                      $rootScope.progressBarShow = false;
                      deferred.reject(msg);
                      utilityService.serviceError(msg.errors[0]);
                   });
                 return deferred.promise;
      },
      verifyOtp : function(data) {

        var deferred = $q.defer();
                 $http({
                            method: "POST",
                            headers: {'Content-Type': 'application/json;'},
                            url:verify_url,
                            data: data
                        })
                   .success(function(data) { 
                      deferred.resolve(data);

                   }).error(function(msg, code) {
                      $rootScope.progressBarShow = false;
                      deferred.reject(msg);
                      utilityService.serviceError(msg.errors[0]);
                   });
                 return deferred.promise;
      },

      verifyPassword : function(data) {

          var deferred = $q.defer();
                 $http({
                            method: "POST",
                            headers: {'Content-Type': 'application/json;'},
                            url:verify_password,
                            data: data
                        })
                   .success(function(data) { 
                      deferred.resolve(data);

                   }).error(function(msg, code) {
                      $rootScope.progressBarShow = false;
                      deferred.reject(msg);
                      utilityService.serviceError(msg.errors[0]);
                   });
                 return deferred.promise;
      },

      resendMailConfirmation : function(data) {

        var deferred = $q.defer();
               $http({
                          method: "PUT",
                          headers: {'Content-Type': 'application/json;'},
                          url:resend_url,
                          data: data
                      })
                 .success(function(data) { 
                    deferred.resolve(data);

                 }).error(function(msg, code) {
                    deferred.reject(msg);
                    $rootScope.progressBarShow = false;
                    utilityService.serviceError(msg.errors[0]);
                 });
               return deferred.promise;
      }

    }

}]);

