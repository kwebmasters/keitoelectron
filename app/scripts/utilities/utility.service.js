angular.module('keitoApp').factory('utilityService', [ '$http','$q', '$log', '$rootScope', '$uibModal',
  function($http,$q,$log,$rootScope,$uibModal) {
    //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
    //var that  = this;
    var myFunctions = {   
      serviceError: function (msg){
                    myFunctions.showAlert(msg);
                  },


      showAlert: function(msg) {
        var modalInstance = $uibModal.open({
            templateUrl: 'views/modals/alert.html',
            controller: 'alertCtrl',
            resolve: {
              msg: function () {
                return msg;
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            $rootScope.progressBarShow = false;        
            $log.info('Modal dismissed at: ' + new Date());

          }, function () {
            $rootScope.progressBarShow = false;
            $log.info('Modal dismissed at: ' + new Date());
          });
      },

      showConfirm: function(msg) {
        var modalInstance = $uibModal.open({
            templateUrl: 'views/modals/confirm.html',
            controller: 'confirmCtrl',
            resolve: {
              msg: function () {
                return msg;
              }
            }
          });
        return modalInstance;
          /*modalInstance.result.then(function (selectedItem) {
            $rootScope.progressBarShow = false;        
            $log.info('Modal dismissed at: ' + new Date());

          }, function () {
            $rootScope.progressBarShow = false;
            $log.info('Modal dismissed at: ' + new Date());
          });*/
      },

      dateFormat: function(epochDate) {
        var epoch = epochDate/1000;
        var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
        d.setUTCSeconds(epoch);
        return d;
      }
    }

    return myFunctions;

}]);

